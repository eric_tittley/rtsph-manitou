#include "SBVH.h"
#include <cassert>
#include <string.h>

/*
#define BINS 32
#define C_TRAV 4
#define C_LEAF 1
#define MAX_LEAFS 2
#define epsilon 1e-6

uint32_t SBVH::cPartition(SBItem_ct& item, SBVHNode_ct& node, real_t& scost) {
    //SAH along longest axis of node.bbox
    Vector3 dv = node.bbox.max-node.bbox.min;
    uint32_t axis = dv.longestAxis();

    //centroid bound
    //NOTE XXX move to partitioning and calculate at level down...
    SmitAABB cb;
    for(uint32_t idx = 0; idx < item.run; ++idx) {
        uint32_t ref = item.off + idx;
        const Sphere& sph = sphs->at(refs[ref]);
        
        cb.expand(sph.center);
    }

    //allocate bins
    SmitAABB bbBins[BINS];
    uint32_t cBins[BINS] = {0};

    //bin all spheres
    for(uint32_t idx = 0; idx < item.run; ++idx) {
        //get sphere
        uint32_t ref = item.off + idx;
        const Sphere& sph = sphs->at(refs[ref]);
        real_t sphca = sph.center[axis];
        Vector3 r3(sph.radius, sph.radius, sph.radius);

        //calculate bin id
        real_t top = sphca - cb.min[axis];
        real_t bot = cb.max[axis] - cb.min[axis];
        uint32_t binid = BINS*(1-epsilon)*top/bot;
        bbBins[binid].expand(sph.center-r3);
        bbBins[binid].expand(sph.center+r3);

        //update bin
        ++cBins[binid];
    }

    //Sweep left
    real_t lsA[BINS] = {0.0};
    uint32_t lsN[BINS] = {0};

    SmitAABB lsBB;
    //XXX lsBB can be negative
    uint32_t lsNt = 0;
    for(uint32_t lidx = 0; lidx < BINS; ++lidx) {
        //Surface area eval
        real_t sa = bbBins[lidx].surfacearea();
        if((sa > 0) && (sa < std::numeric_limits<real_t>::infinity())) {
            lsBB.expand(bbBins[lidx]);
        }
        lsA[lidx] = lsBB.surfacearea();

        //Count eval
        lsNt += cBins[lidx];
        lsN[lidx] = lsNt;
    }

    //Sweep right
    real_t rsA[BINS];
    uint32_t rsN[BINS] = {0};

    SmitAABB rsBB;
    //XXX rsBB can be negative!!!
    uint32_t rsNt = 0;
    for(uint32_t idx = 0; idx < BINS; ++idx) {
        uint32_t ridx = BINS - idx - 1;

        //Surface area eval
        //rsBB.expand(bbBins[ridx]);
        real_t sa = bbBins[ridx].surfacearea();

        if((sa > 0) && (sa < std::numeric_limits<real_t>::infinity())) {
            rsBB.expand(bbBins[ridx]);
        }
        rsA[ridx] = rsBB.surfacearea();

        //Count eval
        rsNt += cBins[ridx];
        rsN[ridx] = rsNt;
    }

    //calculate cost
    real_t cost[BINS] = {0};
    real_t invPA = 1.0/node.bbox.surfacearea();
    for(uint32_t ibin = 0; ibin < BINS; ++ibin) {
        real_t bcost = C_TRAV;

        bcost += lsA[ibin]*invPA*lsN[ibin]*C_LEAF;
        bcost += rsA[ibin]*invPA*rsN[ibin]*C_LEAF;
        
        cost[ibin] = bcost;
    }

    //lowest cost partition
    uint32_t costid = 0;
    real_t mincost = cost[0];
    for(uint32_t idx = 1;idx < BINS; ++idx) {
        if(cost[idx] < mincost) {
            mincost = cost[idx];
            costid = idx;
        }            
    }
    scost = mincost;

    //inplace partitioning
    uint32_t store = item.off;
    for(uint32_t idx = 0; idx < item.run; ++idx) {
        uint32_t ref = item.off+idx;
        const Sphere& sph = sphs->at(refs[ref]);

        real_t top = sph.center[axis] - cb.min[axis];
        real_t bot = cb.max[axis] - cb.min[axis];
        uint32_t binid = BINS*(1-epsilon)*top/bot;

        if(binid < costid) {
            //swap reference
            uint32_t reftmp = refs[ref];
            refs[ref] = refs[store];
            refs[store] = reftmp;

            //inc itertor
            ++store;
        }
    }

    return store;
}
     
void SBVH::cBBox(SBItem_ct& item, SBVHNode& node) {
    SmitAABB bbox;

    for(uint32_t idx = 0; idx < item.run; ++idx) {
        const Sphere& sph = sphs->at(refs[item.off+idx]);

        Vector3 r3(sph.radius, sph.radius, sph.radius);

        bbox.expand(sph.center-r3);
        bbox.expand(sph.center+r3);
    }

    node.bbox = bbox;
}

bool SBVH::construct(spheres_ct& spheres) {
    //check input
    if(!(spheres.size() > 0))
        return false;

    //generate references
    sphs = &spheres;
    for(uint32_t i = 0; i < sphs->size(); ++i) {
        refs.push_back(i);
    }

    //construct SAH BVH
    std::stack<SBItem> buildStack;
   
    //insert root item
    SBItem rootitem;
    rootitem.off = 0;
    rootitem.run = refs.size();
    rootitem.right = false;
    rootitem.root = true;
    buildStack.push(rootitem);

    //Process children
    while(!buildStack.empty()) {
        //get build item
        SBItem item = buildStack.top();
        buildStack.pop();

        //create child
        uint32_t inode = nodes.size();
        SBVHNode node;
        
        //set parent indices
        if(!item.root) {
            if(item.right)
                nodes[item.pnode].right = inode;
            else
                nodes[item.pnode].left = inode;
        }

        //calculate bounding box
        cBBox(item, node);
        //std::clog << "BBox calculated." << std::endl;

        //calculate split cost & split position
        real_t scost;
        //k is offset in ref array for second partition start
        //k-1 is last element in first partition
        uint32_t k = cPartition(item, node, scost);
        //std::cout << "Partition: " << k << std::endl;
        //std::cout << "SCOST " << scost << std::endl;
        
        double lcost = item.run*C_LEAF;
        //std::cout << "LCOST " << lcost << std::endl;

        if(lcost < scost) {
            node.leaf = true;

            node.off = item.off;
            node.run = item.run;
        }
        //internal node
        else {
            node.leaf = false;
            
            //create left item
            SBItem litem; 
            litem.off = item.off; 
            litem.run = k-item.off;
            litem.pnode = inode; 
            litem.right = false;

            //create right item
            SBItem ritem; 
            ritem.off = k;
            ritem.run = item.off+item.run-k;
            ritem.pnode = inode; 
            ritem.right = true;

            //push to build item
            buildStack.push(ritem); buildStack.push(litem);
        }

        //write node
        nodes.push_back(node);
    }

    return true;
}
*/
