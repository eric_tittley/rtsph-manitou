#include <limits>
#include <iostream>
#include <cstdlib>

#include "Sphere.h"
#include "Parser.h"
#include "CBVH.h"

#define RAYS 1000

float randf() {
    return (float)rand()/(float)RAND_MAX;
}

int main() {
    //Load dataset
    //uint32_t sphere_count;
    //Sphere* spheres;
    std::vector<Sphere> spheres;
    parse("data/Data_025", spheres);

    //Generate tree
    CBVH tree;
    tree.construct(&spheres);
    tree.print();

    //Generate rays from center to all spherical directions
    /*AABB& bbox = root->bbox;
    float xc = (bbox.maxX-bbox.minX)*0.5f;
    float yc = (bbox.maxY-bbox.minY)*0.5f;
    float zc = (bbox.maxZ-bbox.minZ)*0.5f;
    Vector3 center(xc,yc,zc);

    float r = (Vector3(bbox.minX,bbox.minY,bbox.minZ)-center).length();*/

    /*Ray rays[RAYS];
    for(uint32_t idx = 0; idx < RAYS; ++idx) {
        Ray& ray = rays[idx];

        //Set origin the same
        ray.origin = center;

        //Calculate direction
        float u = randf(), v = randf();
        float theta = 2*M_PI*u;
        float phi = acos(2*v-1.0f);

        float x = r*sin(theta)*cos(phi);
        float y = r*sin(theta)*sin(phi);
        float z = r*cos(theta);

        ray.direction = Vector3(x,y,z);
    }*/

    //Shoot rays through tree
    /*for(uint32_t idx = 0; idx < RAYS; ++idx) {
        Ray& ray = rays[idx];

        //Search for spheres
        sphidxs_t sphs;
        tree.traversal(ray,std::numeric_limits<float>::infinity(),sphs);

        std::cout << "R:" << idx << ":N:" << nodes.size()<< std::endl;

        //Display
        //for(uint32_t inode = 0; inode < nodes.size(); ++inode) {
            //const BVHNode* node = nodes[inode];


            //print spheres
            //for(uint32_t isph = node->offset; isph < node->offset+node->run; ++isph) {
            //    std::cout << "R:"<<idx<<":"<<isph<<std::endl;
            //}
        //}
    }*/
}
