#include "Ray.h"
#include "Sphere.h"

#include <iostream>
#include <cmath>

int main() {
    Sphere sph;
    sph.center = Vector3(0,0.5,10);
    sph.radius = 1;

    Ray ray;
    ray.origin = Vector3(0,0,5);
    ray.direction = Vector3(0,0,1);
    ray.tmax = 10;

    //project sphere center to ray
    Vector3 trans = sph.center - ray.origin;
    double td = trans.length();
    std::cout << "td " << td << std::endl;

    double tc = ray.direction.dot(trans);
    std::cout << "tc " << tc << std::endl;

    //XXX reduced accuracy
    std::cout << "b " << sqrt(td*td-tc*tc) << std::endl;

    Vector3 prj = ray.origin + ray.direction * tc;
    std::cout << prj.x << " " << prj.y << " " << prj.z;
    std::cout << std::endl;
   
    //accurate
    Vector3 bv = sph.center - prj;
    std::cout << "b " << bv.length() << std::endl;

}
