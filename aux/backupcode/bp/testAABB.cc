#include "AABB.h"
#include <cstdlib>

float randf() {
    return (float)rand()/(float)RAND_MAX;
}

int main() {
    AABB aabb;
    aabb.min = Vector3(-1,-1,-1);
    aabb.max = Vector3( 1, 1, 1);
    aabb.print();

    Ray f0, f1, f2, f3, f4, f5;

    //X, face center
    f0.origin = Vector3(-2, 0, 0); f0.direction = Vector3( 1, 0, 0);
    f1.origin = Vector3( 2, 0, 0); f1.direction = Vector3(-1, 0, 0);
    //Y
    f2.origin = Vector3( 0,-2, 0); f2.direction = Vector3( 0, 1, 0);
    f3.origin = Vector3( 0, 2, 0); f3.direction = Vector3( 0,-1, 0);
    //Z
    f4.origin = Vector3( 0, 0,-2); f4.direction = Vector3( 0, 0, 1);
    f5.origin = Vector3( 0, 0, 2); f5.direction = Vector3( 0, 0,-1);

    //Face hits
    float t0=0, t1=1000;
    bool hit;
    
    hit = aabb.intersect(f0,t0,t1);
    std::cout<<"f0 "<<hit<<std::endl;
    hit = aabb.intersect(f1,t0,t1);
    std::cout<<"f1 "<<hit<<std::endl;
    hit = aabb.intersect(f2,t0,t1);
    std::cout<<"f2 "<<hit<<std::endl;
    hit = aabb.intersect(f3,t0,t1);
    std::cout<<"f3 "<<hit<<std::endl;
    hit = aabb.intersect(f4,t0,t1);
    std::cout<<"f4 "<<hit<<std::endl;
    hit = aabb.intersect(f5,t0,t1);
    std::cout<<"f5 "<<hit<<std::endl;
}
