#include "CBVH.h"

#include <algorithm>
#include <iostream>
#include <cassert>

/*void BVH::calcBBox(uint32_t min, uint32_t max, BVHNode& node) {
    SmitAABB bbox;

    for(uint32_t idx = min; idx <= max; ++idx) {
        const Sphere& sph = sphs->at(refs[idx]);
        Vector3 r3(sph.radius, sph.radius, sph.radius);

        bbox.expand(sph.center-r3);
        bbox.expand(sph.center+r3);
    }

    node.bbox = bbox;
}

struct compSphere {
    uint32_t idx;
    spheres_ct* sphs;

    bool operator()(const uint32_t i, const uint32_t j) const {
        const Sphere& s1 = sphs->at(i);
        const Sphere& s2 = sphs->at(j);
        return (s1.center[idx] < s2.center[idx]);
    }
};

uint32_t BVH::partitionNode(uint32_t min, uint32_t max, const BVHNode& node) {
    //Median Cut along longest axis
    Vector3 dv = node.bbox.max-node.bbox.min;

    sphrefs_t::iterator minit = refs.begin() + min;
    sphrefs_t::iterator maxit = refs.begin() + max+1;

    //Find longest axis
    compSphere comp;
    comp.sphs = sphs;
    if((dv.x>dv.y)&&(dv.x>dv.z)) comp.idx = 0;
    else if(dv.y>dv.z) comp.idx = 1;
    else comp.idx = 2;

    //Sort along axis
    std::sort(minit, maxit, comp);

    //Return object median
    return (min+max)/2;
}

void encode(uint32_t min, uint32_t max, uint32_t& enc) {
    //Check that min index is smaller than largest possible
    assert(min < 0x08000000);
    uint32_t run = max-min;
    assert(run < 0x00000020);

    uint32_t idxmask = 0x07ffffff;
    uint32_t runmask = 0xf8000000;

    enc = 0;
    enc |= min & idxmask;
    enc |= ((run << 27) & runmask);
}

void decode(uint32_t enc, uint32_t& min, uint32_t& max) {
    uint32_t idxmask = 0x07ffffff;
    uint32_t runmask = 0xf8000000;

    min = enc & idxmask;
    uint32_t run = (enc & runmask) >> 27;
    max = run+min;
}    

void BVH::buildRoot(std::stack<BItem>& stack) {
    //item
    BItem item; item.min = 0; item.max = refs.size()-1;

    //root
    size_t inode = 0;
    BVHNode node;
    
    //bounding box
    //TODO set min/max in node, then dont pass
    calcBBox(item.min, item.max, node);

    //leaf
    if((item.max-item.min)<max_idxs) {
        //node.leaf = true;

        //node.idxmin = item.min;
        //node.idxmax = item.max;
        node.rightIdx = 0;
        encode(item.min,item.max,node.enc);
    }
    //children
    else {
        uint32_t k = partitionNode(item.min,item.max,node);
        
        BItem litem; litem.min = item.min; litem.max = k;
        litem.pnode = inode; litem.right = false;

        BItem ritem; ritem.min = k+1; ritem.max = item.max;
        ritem.pnode = inode; ritem.right = true;

        //Zero out node min/max

        //FILO
        stack.push(ritem); stack.push(litem);
    }

    //write node
    nodes.push_back(node);
}

bool CBVH::construct(spheres_ct* spheres) {
    if(!(spheres->size() > 0)) {
        return false;
    }

    //References
    sphs = &spheres;
    for(uint32_t i = 0; i < sphs->size(); ++i) {
        refs.push_back(i);
    }
    //Precompute AABB of every sphere
    //TODO??? store on heap!
    //compute centroid bounds

    //Construction
    std::stack<BItem> buildStack;
   
    //Process root
    buildRoot(buildStack);
   
    //Process children
    while(!buildStack.empty()) {
        //item
        BItem item = buildStack.top();
        buildStack.pop();

        //child
        uint32_t inode = nodes.size();
        BVHNode node;
        
        //parent indices
        if(item.right)
            nodes[item.pnode].rightIdx = inode;

        //bounding box
        calcBBox(item.min, item.max, node);
        
        //leaf, termination criteria, TODO allow for cost!!!
        if((item.max-item.min) < max_idxs) {
            //node.leaf = true;
            //node.idxmin = item.min;
            //node.idxmax = item.max;

            node.rightIdx = 0;
            encode(item.min,item.max,node.enc);
            //node.refs = encode(item.min,item.max);
        }
        //node
        else {
            uint32_t k = partitionNode(item.min, item.max, node);
            
            BItem litem; litem.min = item.min; 
            litem.max = k;
            //litem.max = item.min+k;
            litem.pnode = inode; litem.right = false;

            BItem ritem; 
            //ritem.min = item.min+k+1; 
            ritem.min = k+1;
            ritem.max = item.max;
            ritem.pnode = inode; ritem.right = true;

            //FILO
            buildStack.push(ritem); buildStack.push(litem);
        }

        //write node
        nodes.push_back(node);
    }

    return true;
}*/
