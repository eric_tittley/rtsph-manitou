#pragma once

#include <vector>
#include <stack>

#include "Common.h"
#include "Tree.h"
#include "AABB.h"

/*struct SBItem {
    uint32_t off, run, pnode;
    bool right, root;
};
typedef const SBItem SBItem_ct;

struct SBVHNode {
    //TODO compress to 32B
    SmitAABB bbox; //48B

    union { //4B
        uint32_t off, left;
    };

    union { //4B
        uint32_t run, right;
    };
    
    bool leaf; //1B
};
typedef const SBVHNode SBVHNode_ct;

class SBVH: public Tree {
public:
    bool construct(spheres_ct&);
    void traverse(const Ray&, sphrefs_t&) const;
    void print() const;

private:
    void cBBox(SBItem_ct&, SBVHNode&);
    uint32_t cPartition(SBItem_ct&, SBVHNode_ct&, real_t&);
    
    //void buildRoot(std::stack<BItem>&);

    //Sphere indices
    spheres_ct* sphs;
    std::vector<uint32_t> refs;

    //Tree nodes
    std::vector<SBVHNode> nodes;
};*/
