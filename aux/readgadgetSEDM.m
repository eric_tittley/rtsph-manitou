% readgadgetSEDM: Read a Gadget files produced by Peter (Woitek?)'s version
% of Gadget
%
% [D,hdr]=readgadgetSEDM(fname,ICflag)
%
% ARGUMENTS
%  Input
%   fname   The name of the file to read
%   ICflag  1 if the file is an IC file [optional, default 0]
%
% RETURNS
%  D       The data in the file, returned as a structure
%  hdr     The header of the file, returned as a structure
%
%  Elements of hdr:
%   npart
%   mass
%   time
%   redshift
%   sft
%   feedback
%   npartTotal
%   flag_cooling
%   num_files
%   BoxSize
%   Omega0
%   OmegaLambda
%   HubbleParam
%   flag_stellarage
%   flag_metals
%   hashtabsize
%
%  Elements of D
%   r
%   v
%   id
%   mass
%   u
%   rho
%   nelec	Electron number density
%   nh		Neutral gas fraction
%   h		SPH smoothing length
%   sfr		Star formation rate
%   age_stars	Age of stars
%   BH_mass	Black Hole masses
%   BH_mdot	Black Hole accretion rates
%
% SEE ALSO
%  ReadGadgetField

% AUTHOR: Eric Tittley (based on script included in Gadget distribution)
%
% COMPATIBILITY: Matlab Octave
%
% HISTORY
%  04 10 08 First version.
%  07 10 12 Ported to Matlab.
%  10 10 08 Changed 4,'char' to 1,'uint' in the padding reads, as Matlab was
%       getting lost on some files.
%  11 12 07 Added extended header fields

function [D,hdr]=readgadgetSEDM(fname,ICflag)

% Default behaviour
if(nargin < 2)
 ICflag=0;
end

% Open the file
fid=fopen(fname,'rb');
if(fid<=0)
 disp(['ERROR: readgadgetSEDM: unable to open file: ',fname])
 return
end

% Read the header
hdr=ReadGadgetHeader(fid);

% The base data, position, velocity, and particle type
N=sum(hdr.npart);

blocksize=fread(fid,1,'uint');
D.r = fread(fid,[3,N],'float');
blocksize=fread(fid,1,'uint');

blocksize=fread(fid,1,'uint');
D.v = fread(fid,[3,N],'float');
blocksize=fread(fid,1,'uint');

blocksize=fread(fid,1,'uint');
D.id = fread(fid,N,'uint');
blocksize=fread(fid,1,'uint');

% The mass, if necessary
ind=find((hdr.npart > 0) & (hdr.mass == 0));
if(~isempty(ind))
 Nwithmass=sum(hdr.npart(ind));
 blocksize=fread(fid,1,'uint');
 D.mass = fread(fid,Nwithmass,'float');
 blocksize=fread(fid,1,'uint');
end

% For the gas data
Ngas=hdr.npart(1);
if(Ngas > 0)
 blocksize=fread(fid,1,'uint');
 D.u = fread(fid,Ngas,'float');
 blocksize=fread(fid,1,'uint');
 if(ICflag ~= 1)
  % IC files don't have other data
  blocksize=fread(fid,1,'uint');
  D.rho = fread(fid,Ngas,'float');
  blocksize=fread(fid,1,'uint');
  blocksize=fread(fid,1,'uint');
  D.nelec = fread(fid,Ngas,'float');
  blocksize=fread(fid,1,'uint');
  blocksize=fread(fid,1,'uint');
  D.nh = fread(fid,Ngas,'float');
  blocksize=fread(fid,1,'uint');
  blocksize=fread(fid,1,'uint');
  D.h = fread(fid,Ngas,'float');
  blocksize=fread(fid,1,'uint');
  blocksize=fread(fid,1,'uint');
  D.sfr = fread(fid,Ngas,'float');
  blocksize=fread(fid,1,'uint');
 end
end

if(ICflag ~= 1)
 % For the stellar data
 Nstars=hdr.npart(5);
 if(Nstars > 0)
  blocksize=fread(fid,1,'uint');
  D.age_stars = fread(fid,Nstars,'float');
  blocksize=fread(fid,1,'uint');
 end

 % For the black holes
 Nbh=hdr.npart(6);
 if(Nbh > 0)
  blocksize=fread(fid,1,'uint');
  D.BH_mass = fread(fid,Nbh,'float');
  blocksize=fread(fid,1,'uint');
  blocksize=fread(fid,1,'uint');
  D.BH_mdot = fread(fid,Nbh,'float');
  blocksize=fread(fid,1,'uint');
 end

end
% Close the file
fclose(fid);
