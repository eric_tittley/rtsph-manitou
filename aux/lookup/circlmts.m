function [l, h] = circlmts(radius, line)
	tmp = sqrt((radius^2) - (line^2));
	l = -tmp;
	h = tmp;
end
