%control
radius = 1.0;
hs = 0.0:0.04:radius;
n=10;
dh = 0.0001;

%private
data = zeros(size(hs,1),7);

format long g

sample = 1;
for ht = hs
    height2 = ht;
    height = sqrt(ht);

	%write sample
	data(sample,1) = sample;
	%height**2
	data(sample,2) = height2 / (radius*radius);
	%write height
	data(sample,3) = height / radius;

	%calculate interval from circle-line intersect
	[low, high] = circlmts(radius,height);
	%write intersects
	%data(sample,4) = low;
	%data(sample,5) = high;
	
	%integrate along interval
	x2r = @(x,y)sqrt(x.^2+y.^2);

	kernel = @(x,y)gspline(x2r(x,y),radius);
	%kernel = @(x,y)cubspline(x2r(x,y),radius);

	%res = integral(@(x)handle(x,height), 0, high, 'ArrayValued', true);

    weight = @(h)quad(@(x)kernel(x,h), 0, high);
	
	%write result
    res = weight(height);
    res2 = res*2.0;
	data(sample,6) = res2;

    %lin
    %diff = @(h)(weight(h+dh)-weight(h-dh))/dh;
    %data(sample,7) = diff(height);

	sample = sample + 1;
end

%derivative calculations


disp('Data');
disp('    Sample    NB2       NB        Low       High      Integral');
disp(data);

disp('Verfiy');
dl = radius/n;
da = dl^2;

rs = [];
for i = dl:dl:radius
	for j = dl:dl:radius
		rs = [rs, sqrt((i-dl/2)^2+(j-dl/2)^2)];
	end
end

grid = interp1(data(:,3),data(:,6),rs,'linear',0.0);

grid = grid .* da .* 4.0;

disp('HAS TO BE 1:');
disp(sum(grid));
