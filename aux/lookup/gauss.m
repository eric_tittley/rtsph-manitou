function weight = gauss(x,h)
	coef = 1.0/(pi^1.5)/(h^3);
	weight = exp(-x.^2/h^2);
end
