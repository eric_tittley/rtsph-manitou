#include "Spheres.h"



/*#if defined __SIMD__
void Spheres::intersect(size_t idx, const Packet& packet, PacketIntersect& intrst) const {
    //input check
    assert(0 <= idx);
    assert(idx < count());

    //translate
    doublew cxw = set1pd(cx[idx]);
    doublew cyw = set1pd(cy[idx]);
    doublew czw = set1pd(cz[idx]);
    doublew h2w = set1pd(h2(idx));
    doublew invh2w = divpd(one, h2w);

    for(size_t i = 0; i < PACKET_SIZE; ++i) {
        //object space
        // l = ray.o - sphere.c
        doublew lx = subpd(packet.ox[i], cxw);
        doublew ly = subpd(packet.oy[i], cyw);
        doublew lz = subpd(packet.oz[i], czw);

        //quadratic coefficents
        // a = d . d = 1 //normalized
        // b = l . ray.d
        doublew lxdx = mulpd(lx, packet.dx[i]);
        doublew lydy = mulpd(ly, packet.dy[i]);
        doublew lzdz = mulpd(lz, packet.dz[i]);
        doublew b = addpd(lxdx, addpd(lydy, lzdz));

        // c = (l . l) - r2
        doublew lx2 = mulpd(lx, lx);
        doublew ly2 = mulpd(ly, ly);
        doublew lz2 = mulpd(lz, lz);
        doublew c = addpd(addpd(lx2, ly2), subpd(lz2, h2w));

        //quadratic discriminant
        doublew b2 = mulpd(b, b);
        doublew disc = subpd(b2, c);
        doublew dmask = cmpltpd(zero, disc);

        //quadratic formula
        doublew sdisc = sqrtpd(disc);

        //calculate (bv - c)
        doublew neb = subpd(zero, b);
        doublew bcx = addpd(mulpd(packet.dx[i], neb), lx);
        doublew bcy = addpd(mulpd(packet.dy[i], neb), ly);
        doublew bcz = addpd(mulpd(packet.dz[i], neb), lz);

        //calculate impact parameter squared (b2)
        doublew bcx2 = mulpd(bcx, bcx);
        doublew bcy2 = mulpd(bcy, bcy);
        doublew bcz2 = mulpd(bcz, bcz);
        doublew ip2 = addpd(bcx2, addpd(bcy2, bcz2));
        intrst.nb2[i] = mulpd(ip2, invh2w);
        
        //solutions
        intrst.t0[i] = mulpd(negone, addpd(b, sdisc));
        intrst.t1[i] = subpd(sdisc, b);

        //packet mask
        doublew xmask = cmplepd(intrst.t0[i], packet.tmax[i]);
        //doublew nmask = cmplepd(packet.tmin[i], intrst.t1[i]);
        doublew nmask = cmplepd(zero, intrst.t1[i]);

        doublew rmask = andpd(xmask, nmask);

        //combined mask
        intrst.active[i] = andpd(dmask, rmask);
    }
}
#endif*/
