#include "BVH.h"
#include <stack>
#include <fstream>

using namespace std;

//SAH parameters
#define BINS 128

//internal data
struct ObjectSplit {
    uint32_t bin, axis;
    double cost;
};

struct SAHItem {
    AABB cb, bb;
    uint32_t off, run, pnode;
    bool right, root;
};

//internal prototypes
ObjectSplit evalSAH(const SAHItem&, const Spheres&, const uint32_t*, const BVHNode&);
void partitionSAH(const ObjectSplit&, const SAHItem&, const Spheres&, uint32_t*, SAHItem&, SAHItem&);

uint32_t binID(const SAHItem&, const ObjectSplit&, const Vector3&);

//definitions
void mkSAHBVH(const Spheres& sphs, BVH& bvh) {
  

    //reference spheres
    uint32_t *refs = new uint32_t[sphs.count()];
    for(uint32_t i = 0; i < sphs.count(); ++i)
        refs[i] = i;

    //construct SAH BVH
    std::stack<SAHItem> stack;
   
    //insert root item
    SAHItem rtitem;
    rtitem.off = 0; rtitem.run = sphs.count();
    rtitem.right = false; rtitem.root = true;
    rtitem.pnode = 0;
    AABB rtbb, rtcb;
    for(uint32_t inc = 0; inc < rtitem.run; ++inc) {
        uint32_t idx = rtitem.off + inc;
        uint32_t ref = refs[idx];
        
        Vector3 cen(sphs.cx[ref], sphs.cy[ref], sphs.cz[ref]);
        Vector3 r3(sphs.h[ref]);
      
        rtcb.expand(cen);
        cen -= r3;
        rtbb.expand(cen);
        r3 *= 2.0;
        cen += r3;
        rtbb.expand(cen);
    }
    rtitem.bb = rtbb; rtitem.cb = rtcb;
    stack.push(rtitem);

    
    //build all items
    while(!stack.empty()) {
   
       //get build item
        SAHItem item = stack.top(); 
        stack.pop();

        //create child
        uint32_t inode = (uint32_t)bvh.nodes.size();
        BVHNode node; node.bbox = item.bb;
        
        //set parent indices
        if(!item.root) {
            if(item.right)
                bvh.nodes[item.pnode].right = inode;
        }

        //calculate split cost & split position
        ObjectSplit split = evalSAH(item, sphs, refs, node);
        
        //calculate leaf cost
        double lcost = item.run * C_LEAF;

        if((lcost < split.cost) || (item.run <= 64)) {
        //if(lcost < split.cost) {
            //create leaf
            node.offset = item.off;
            node.run = item.run;

            //copy data in required order
            for(uint32_t i = 0; i < item.run; ++i) {
                uint32_t idx = item.off + i;
                uint32_t ref = refs[idx];
               
                //insert in idx
                bvh.spheres.cx.push_back(sphs.cx[ref]);
                bvh.spheres.cy.push_back(sphs.cy[ref]);
                bvh.spheres.cz.push_back(sphs.cz[ref]);
                bvh.spheres.h.push_back(sphs.h[ref]);
                bvh.spheres.invh2.push_back(sphs.invh2[ref]);
            }
        }
        //internal node
        else {
            node.setInternal();

            //apply partition
            SAHItem litem, ritem; 
            //partitionSAH(split, item, spheres, refs, litem, ritem);
            partitionSAH(split, item, sphs, refs, litem, ritem);
            
            //create left item
            litem.root = false;
            litem.pnode = inode; 
            litem.right = false;

            //create right item
            ritem.root = false;
            ritem.pnode = inode; 
            ritem.right = true;

            //push to build item
            stack.push(ritem); stack.push(litem);
        }

        //write node
        bvh.nodes.push_back(node);
    }

    //copy header
    //bvh.spheres.copy(sphs);

    //copy mapping
    for(size_t i = 0; i < sphs.count(); ++i) {
        bvh.map.push_back(refs[i]);
    }

    delete[] refs;
    
}

uint32_t binID(const SAHItem& item, const ObjectSplit& split, const Vector3& vec) {
    assert(vec[split.axis] >= item.cb.bounds[0][split.axis]);
    assert(vec[split.axis] <= item.cb.bounds[1][split.axis]);
    
    double top = vec[split.axis] - item.cb.bounds[0][split.axis];
    double bot = item.cb.bounds[1][split.axis] - item.cb.bounds[0][split.axis];
    return BINS * (1.0 - bepsilon) * top / bot;
}

//ObjectSplit evalSAH(const SAHItem& item, Sphere* spheres, uint32_t* refs, const BVHNode& node) {
ObjectSplit evalSAH(const SAHItem& item, const Spheres& sphs, const uint32_t* refs, const BVHNode& node) {
    //input check
    assert(item.run > 0);

    //output
    ObjectSplit split[3];

    //axis selection
    //for(uint32_t axis = 0; axis < 3; ++axis) {
        Vector3 dv;
        dv += item.bb.bounds[1];
        dv -= item.bb.bounds[0];

        uint32_t axis = dv.longestAxis();
        split[axis].axis = axis;

        //binned SAH calculation
        AABB binBB[BINS];
        uint32_t binCS[BINS] = {0};





        //bin spheres
        for(uint32_t inc = 0; inc < item.run; ++inc) {
	    
	    //get sphere
            uint32_t idx = item.off + inc;
            uint32_t ref = refs[idx];
            
            //const Sphere& sph = spheres[ref];
            //Vector3 cen(sph.c);
            //Vector3 r3(sph.r);
	 

            Vector3 cen(sphs.cx[ref], sphs.cy[ref], sphs.cz[ref]);
            Vector3 r3(sphs.h[ref]);
            
            //calculate bin id
            uint32_t bid = binID(item, split[axis], cen);

            //update bin
            cen -= r3;
            binBB[bid].expand(cen);
            r3 *= 2.0;
            cen += r3;
            binBB[bid].expand(cen);

            binCS[bid]++;
        }
	

        //aggregating sweep
        double lA[BINS], rA[BINS];
        uint32_t lC[BINS], rC[BINS];

        AABB lBB, rBB;
        uint32_t lCT = 0, rCT = 0;

        for(uint32_t bin = 0; bin < BINS; ++bin) {
            uint32_t lbin = bin, rbin = BINS-1-bin;

            //process bounds 
            lBB.expand(binBB[lbin]);
            lA[lbin] = lBB.surfacearea();
	    
            rBB.expand(binBB[rbin]);
            rA[rbin] = rBB.surfacearea();

            //process counts
            lCT += binCS[lbin];
            lC[lbin] = lCT;

            rCT += binCS[rbin];
            rC[rbin] = rCT;
        }

        //optimal cost sweep
        double invLC = C_LEAF / node.bbox.surfacearea();

        //unroll first bin
        split[axis].cost = C_TRAV + (lA[0]*lC[0] + rA[0]*rC[0])*invLC;
        split[axis].bin = 0;

        //process all other bins
        for(uint32_t bin = 1; bin < BINS; ++bin) {
            //calculate cost
            double cost = C_TRAV;
            cost += (lA[bin]*lC[bin] + rA[bin]*rC[bin])*invLC;

            //compare to minimum
            if(cost < split[axis].cost) {
                split[axis].cost = cost;
                split[axis].bin = bin;
            }
        }

        return split[axis];
    //}

    //find least cost axis
    //if((split[0].cost < split[1].cost) && (split[0].cost < split[2].cost)) {
    //    return split[0];
    //}
    //else if(split[1].cost < split[2].cost) {
    //    return split[1];
    //}
    //else {
    //    return split[2];
    //}

	//myfile.close();
}

//void partitionSAH(const ObjectSplit& split, const SAHItem& item, Sphere* spheres, uint32_t* refs, SAHItem& left, SAHItem& right) {
void partitionSAH(const ObjectSplit& split, const SAHItem& item, const Spheres& sphs, uint32_t* refs, SAHItem& left, SAHItem& right) {
    //partition list into two groups
    uint32_t store = item.off;
    for(uint32_t inc = 0; inc < item.run; ++inc) {
        uint32_t idx = item.off + inc;
        uint32_t ref = refs[idx];
        
        //Sphere& sph = spheres[ref];
        Vector3 cen(sphs.cx[ref], sphs.cy[ref], sphs.cz[ref]);

        //uint32_t bid = binID(item, split, sph.c);
        uint32_t bid = binID(item, split, cen);

        if(bid < split.bin) {
            //swap reference
            uint32_t reftmp = refs[idx];
            refs[idx] = refs[store];
            refs[store] = reftmp;

            //inc itertor
            ++store;
        }
    }
    
    assert(store > 0);
    assert(store < (item.off + item.run));

    //assign left and right items
    left.off = item.off;
    left.run = store - item.off;

    right.off = store;
    right.run = item.off + item.run - store;

    //bounding boxes and centroid boxes
    AABB lcb, lbb;
    for(uint32_t inc = 0; inc < left.run; ++inc) {
        uint32_t idx = left.off + inc;
        uint32_t ref = refs[idx];
        
        //const Sphere& sph = spheres[ref];
        //Vector3 cen(sph.c);
        //Vector3 r3(sph.r);
 
        Vector3 cen(sphs.cx[ref], sphs.cy[ref], sphs.cz[ref]);
        Vector3 r3(sphs.h[ref]);

        lcb.expand(cen);
        cen -= r3;
        lbb.expand(cen);
        r3 *= 2.0;
        cen += r3;
        lbb.expand(cen);
    }
    left.bb = lbb; left.cb = lcb;

    AABB rcb, rbb;
    for(uint32_t inc = 0; inc < right.run; ++inc) {
        uint32_t idx = right.off + inc;
        uint32_t ref = refs[idx];
        
        //const Sphere& sph = spheres[ref];
        //Vector3 cen(sph.c);
        //Vector3 r3(sph.r);

        Vector3 cen(sphs.cx[ref], sphs.cy[ref], sphs.cz[ref]);
        Vector3 r3(sphs.h[ref]);
      
        rcb.expand(cen);
        cen -= r3;
        rbb.expand(cen);
        r3 *= 2.0;
        cen += r3;
        rbb.expand(cen);
    }
    right.bb = rbb; right.cb = rcb;
}
