\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Goal of Project}{2}{subsection.1.1}
\contentsline {section}{\numberline {2}Background}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Smoothed Particle Hydrodynamics}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Radiative Transfer methods}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Ray Tracing}{5}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Acceleration Structures}{5}{subsubsection.2.3.1}
\contentsline {section}{\numberline {3}Method}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Rays \& Packets}{7}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Ray Packing}{7}{subsubsection.3.1.1}
\contentsline {subsection}{\numberline {3.2}Bounding Volume Hierarchy}{8}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Tree Construction \& Surface Area Heuristic}{9}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Tree Traversal}{12}{subsubsection.3.2.2}
\contentsline {subsection}{\numberline {3.3}Column Density Calculation}{15}{subsection.3.3}
\contentsline {section}{\numberline {4}Optimization}{17}{section.4}
\contentsline {subsection}{\numberline {4.1}Data Layout}{17}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Lookup Table}{18}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Parallelism}{18}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Instruction Level Parallelism}{18}{subsubsection.4.3.1}
\contentsline {section}{\numberline {5}Verification \& Performance}{19}{section.5}
\contentsline {subsection}{\numberline {5.1}Tree Construction Verification}{19}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Column Density Verification}{21}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Performance Analysis}{22}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Parallel Performance}{23}{subsection.5.4}
\contentsline {section}{\numberline {6}Uses}{29}{section.6}
\contentsline {subsection}{\numberline {6.1}Absorption by cold dust}{29}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Reionisation of the intergalactic medium}{29}{subsection.6.2}
\contentsline {section}{\numberline {7}Discussion \& Further Work}{30}{section.7}
\contentsline {section}{\numberline {8}Conclusion}{32}{section.8}
\contentsline {section}{\numberline {9}Acknowledgements}{33}{section.9}
\contentsline {section}{\numberline {10}References}{33}{section.10}
\contentsline {section}{Appendices}{35}{section*.1}
\contentsline {section}{\numberline {A}Kernel Look Up Table}{35}{Appendix.1.A}
\contentsline {section}{\numberline {B}Source Code Access}{35}{Appendix.1.B}
