#! /usr/bin/python2

import numpy as np
from struct import unpack
import matplotlib.pyplot as plt
import matplotlib.mlab as ml
from matplotlib.colors import LogNorm
from mpl_toolkits.mplot3d import Axes3D

fid = open("unit.cds","rb")
nx = unpack("L", fid.read(8))[0]
ny = unpack("L", fid.read(8))[0]
den = np.fromfile(fid, dtype='double', count=nx*ny)
ren = np.reshape(den, (ny,nx))
fid.close()

ref = np.loadtxt("ref.txt")

frac = (ren - ref)/ref;

plt.imshow(frac, norm=LogNorm(), cmap='jet')

plt.xlabel("Spatial X Axis (20 KPc)")
plt.ylabel("Spatial Y Axis (20 KPc)")

cbar = plt.colorbar()
cbar.ax.set_ylabel("Fractional Error")

fig = plt.gcf()
fig.set_size_inches(10,10)

plt.savefig("../graphics/fracerr.png", format="png")
