#! /usr/bin/python2

import numpy as np
from struct import unpack
import matplotlib.pyplot as plt
import matplotlib.mlab as ml
from matplotlib.colors import LogNorm
from mpl_toolkits.mplot3d import Axes3D

fid = open("tree.cds","rb")
nx = unpack("L", fid.read(8))[0]
ny = unpack("L", fid.read(8))[0]
den = np.fromfile(fid, dtype='double', count=nx*ny)
ren= np.reshape(den, (ny,nx))
fid.close()

plt.imshow(ren, cmap='jet')

plt.xlabel("Spatial X Axis (10 KPc)")
plt.ylabel("Spatial Y Axis (10 KPc)")

cbar = plt.colorbar()
cbar.ax.set_ylabel("Node Intersections")

plt.savefig("../graphics/treemap.png", format="png")
