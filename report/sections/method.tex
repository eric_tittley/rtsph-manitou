%short summary
The algorithm developed for this project can be broadly split into
three sections, tree building, tree traversal and column density
calculation. While the ultimate goal is fulfilled by the third part
calculating the column density of particles which are intersected 
with a ray it would be far to slow to use as a naive method. The 
complexity of a ray tracing algorithm \(N^2\) due to the fact that
for each ray each particle could be intersected and therefore each
particle must be atleast checked for an intersection. While for low
numbers of rays or particles a naive implementation is an adequate 
solution for large number of either rays or spheres this becomes
an unfeasible approach.

%tree summary
The tree structure included in the algorithm to calculate the final
column density is a purely algorithmic optimization which allows
a fast method of excluding large amounts of particles to be tested.
While the tree building requires additional time which is not
present in the naive version of the algorithm the acceleration
achieved by excluding particles is much more important. The tree 
building can be considered a form of sorting in a complex 
multidimensional and constraint object space trying to find an 
optimal configuration which allows fast querying. When queries to 
the tree are performed it is important to have a fast method of 
traversal since this procedure is performed for each ray.

%integration summary
In addition to the tree traversal the actual calculation of the
column density is performed through a look up table instead of being
calculated at run time, because numerical integrations are expensive
operations. 

\subsection{Rays \& Packets} \label{sec:rps}

The algorithm was developed to calculate the column density along a
ray which is directed through a SPH simulation of gas. Rays offer a
great amount of flexibility to the calculation due to the complete
independence to other rays. The traversal of rays through a Bounding
Volume Hierarchy(BVH) is also simple since no dependence except for 
the tree is present. All rays in the method are entirely separable, 
however in the runtime environment the rays are not separate anymore
this is due to resource contention on a single central processing 
unit(CPU) which might be processing several rays at once with 
multiple threads. This leads to each thread requiring cache memory 
space and processing units on the CPU which must be shared. Sharing
cache memory can be difficult if separate rays are not touching 
similar memory areas, which causes cache lines to be swapped into 
main memory often.

In addition to rays the algorithm implemented supports ray packets.
These are collections of rays identical to a group of rays being
traced individually. Packets are a multiple of the Single 
Instruction Multiple Data(SIMD) width usually of the size of 16 to
64 rays per packet. While it is more complex and less readable to
use packets due to the SIMD instructions which are used to 
calculate the rays in parallel on the instruction level it is 
beneficial, because the memory used is shared between a large number
of rays and therefore there is less competition for resources of the
CPU.

\subsubsection{Ray Packing} \label{sec:rpc}

\begin{wrapfigure}{r}{0.5\textwidth}
    \begin{algorithm}[H]
        \KwIn{N 6D points}
        \KwResult{packets \& rays}

        allocate N usage flags for points
        KD tree construction from given point set
        
        \While{more neighbors in radius than packet size}{
            select a point
            find packet size neighbors including query point
            flag all selected points
            package the points into packets
        }

        extract remaining points are individual rays
    \end{algorithm}
    \caption{Pseudo code of packing rays using a KD-Tree.}
    \label{alg:packing}
\end{wrapfigure}

Since packets are superior to rays it is beneficial to use packets
whenever possible, however this is not simple if rays are not 
generated from a coherent set of points which are easily translated
into a coherent set of rays. When rays are generated for purposes
of radiative transfer it is not possible to guarantee any coherency
at all. On the other hand when traditional images are rendered
pixels close to each other are ideal sources of coherency at which
point this method is not required.

The solution presented and implemented to pack rays into packets is
to use a nearest neighbor search algorithm in a six dimensional 
space. Specifically a KD-Tree was filled with two sets of three 
dimensional points from which the rays were generated and the 
standard KD-Tree nearest neighbor search was used for extracting
k-nearest neighbor points and filling packets with the corresponding
rays.

In figure \ref{alg:packing} the pseudo code is presented to make 
clear how this functions with a KD-Tree. While this problem is a 
k-nearest neighbor search the use of a KD-Tree is not an optimal 
solution to the given problem, because the set of points varies 
when packets are extracted from the tree. KD-Trees are notoriously 
difficult to build fast with high quality and therefore a more 
flexible scheme would be advantageous. 

The method requires fast deletion of points in the spatial index 
and an efficient k-nearest neighbor search algorithm with a radius.
Ideally a perfect nearest neighbor search would be present, however
it is not required as long as the points are within an 
implementation defined radius for efficiency. The performance of
imperfect nearest neighbor rays would likely be masked within the 
processing of a packet.

\subsection{Bounding Volume Hierarchy} \label{sec:bvh}

The Bounding Volume Hierarchy(BVH) was selected as the tree 
algorithm compared to all other possible solutions. The selection of
an object subdivision scheme instead of a simpler to understand 
spatially splitting scheme is due to the requirement of not double
counting spheres intersected by rays. A spatial splitting tree 
has references to object in each node which the node it overlaps 
with. With graphics based ray tracing this is not a problem, because
only the first intersection of any sphere is important. In our 
algorithm it is required that all spheres intersecting the ray are
found. When a spatially splitting tree is used an extra phase to
find all intersections is required which eliminates duplicate 
entries in the final object list of intersecting spheres.

This duplicate reduction can be achieved with traditional ray
tracing methods called mailboxing or with a computer science
method for reducing duplicates in a list. The mailboxing algorithm
flags when a ray intersects a specific sphere and if the same 
sphere is tested the flag is checked to determine whether it should
be counted. The original method required a matrix of size of all
spheres and rays to represent the flags. This is not efficient for 
use since when the number of either is large a large memory
footprint is required only for removal of duplicates.  Several 
adaptations exist which allow less memory to be used, however they
no longer guarantee that all duplicates are found. Sparse matrix
representations such as Coordinate Matricies(COO) or Compressed
Sparse Row(CSR) could be used for the naive method which would 
reduce the memory requirement to a large extent, however this
method is also not helpful because for every ray traced the matrix
would have to be searched to find if the sphere has been flagged by
the specific ray.

The list duplicate removal from computer science which guarantees 
all requirements involves a sort of the entire list and then 
ignoring all elements which are equivalent to the previous item. 
This approach is also not optimal since a sort of a list is of 
order \(O(N\log{N})\), which is also executed for every ray which 
is traced.

%BVH properties
Considering the BVH is not a spatially splitting tree it provides 
the property that no references to particles are duplicated in the 
entirety of the tree. Therefore when the tree is used to find 
intersections no duplicates can be reported by the ray intersection.
This eliminates the need for a duplicate removal phase and therefore
saves time for each ray which can be a substantial cost when very 
large numbers of rays are used.

\subsubsection{Tree Construction \& Surface Area Heuristic}
\label{sec:con}

%general building bvh
Building a Bounding Volume Hierarchy(BVH) is generally considered a 
difficult task. No single algorithm is ideal for all situations and
no algorithm has been determined to produce the best quality tree
for all situations. The build quality is a extremely important
metric, it describes how costly it is to traverse the tree and find
the solution to the spatial query. Building low quality trees is
simple, however the time saved while building the tree and the ease
comes at the cost of a higher traversal time for any ray. The cost
of the BVH is determined by the amount of spatial overlap between
internal nodes and how many traversal steps are involved in finding
the nodes of interest to the query.

\begin{wrapfigure}{r}{0.5\textwidth}
    \begin{algorithm}[H]
        \KwIn{geometric dataset}
        \KwData{build item stack}
        \KwResult{BVH node data}

        push root build item onto stack

        \While{build stack is not empty}{
            pop current build item of stack
            
            \If{termination criteria reached}{
                create leaf node from current build item \\
            }

            find object partition \\
            create internal node from current build item \\
            create push child items onto stack \\
        }
    \end{algorithm}
    \caption{Pseudo code of BVH top-down build algorithm.}
    \label{alg:build}
\end{wrapfigure}

%top down building
Since the number of possible trees from an input set increases
exponentially with the input sets size it is not feasible to test
every tree for being optimal. Therefore three strategies exist to 
build a BVH top-down, bottom-up and insertion. These are illustrated
in figure \ref{fig:str}. The top-down strategy
is the most commonly used, because it is simple to understand and 
algorithms for actually performing such a build have been well 
researched. When the top-down method is used the entire dataset is 
present at the beginning in a single root node. The dataset is then 
continually subdivided within this root node into other internal 
nodes which are pointed to by the root node. Eventually a 
termination criteria is reached which terminates further 
subdivision. The choice of how to subdivide the objects determines 
the quality of the bounding volume hierarchy.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{graphics/strategies}
    \caption{Build strategies of a bounding volume hierarchy.}
    \label{fig:str}
\end{figure}

%bottom-up building
The bottom-up approach is more difficult to implement and is not
used often. When the entire dataset is present the objects are 
formed into groups and encase in the bounding volume of choice.
Then once all objects are encased the new set of bounding volumes
is considered the same as the initial set and also grouped into 
their bounding volumes, this leads to a BVH. The difficulty in this
approach is to determine the groups into which the objects should be
put. Generally when it is possible to build trees in this fashion
they produce extremely high quality, since the spatial locality of
all the volumes is guaranteed by the algorithm and not only by
the choice of the subdivision.

%insertion based building
Finally the last approach is the insertion based tree, separate
from the other methods which require the entire dataset to be 
present this method does not. The tree is built continually as more
of the dataset is provided, while this is a good approach for this
situation the build quality is severely damaged, because the spatial
locality can not be given to nodes unless an entire rebuild is done
with all objects which have been given.

%branching factor
Bounding volume hierarchies usually have a branching factor of two,
meaning that an internal node has two children to which it points.
Other methods as discussed in section \ref{sec:dis} make use of 
higher branching factors which can be beneficial to the traversal 
cost, however no known algorithms produce such trees. Building 
bounding volume hierarchies with higher branching factors is done 
by building a tree of branching factor two and then combining 
multiple levels of the tree into single internal nodes pointing to 
all the children of the original binary tree.

%partitioning strategies
The strategy used in our implementation of the BVH is the top-down
method. The pseudo code in figure \ref{alg:build} shows the general
method of a top-down BVH build procedure. A build item as shown
in the algorithm is a data structure which describes the current
state to which the tree is built. Build items contain the geometric
shapes which are still to be subdivided and information about where
the new node will be placed inside the tree.

%partition, axis
As seen in figure \ref{alg:build} and important part of the build
other than the terminating criteria is finding a object partition.
Many different choices exist to decide where to divide the
set of objects. For the build quality to be as high as possible
a good division method has to be chosen which is appropriate to the 
geometry. Properties which are considered beneficial for a BVH can 
be used to make such a choice such as minimizing the volume of the 
child nodes, minimizing the overlap of child nodes or dividing 
particles equally among children. Also the axis in which to 
subdivide the set is a complex choice. Usually either all axes are 
evaluated, a simple choice based on a round robin fashion of 
distribution is used or the largest axis of the volume is used 
since it should have largest variance and therefore easiest to 
subdivide into two clear sets.

\begin{wrapfigure}{t}{0.5\textwidth}
    \begin{algorithm}[H]
        \KwIn{Build Item}
        \KwData{bin bounds, bin counters, bin surface areas}
        \KwResult{ObjectSplit data}

        \tcp{find largest axis of build item}
        
        \tcp{bin all spheres in item}
        \ForEach{sphere in item}{
            calculate bin id of sphere \\
            expand selected bin by bounding box \\
            increment bin counter \\
        }

        \tcp{aggregation sweeps}
        \ForEach{bin}{
            expand temporary bounds from both sides \\
            increment temporary counters from both sides \\
            set bin surface area from temporary bound \\
            set bin counter from temporary counter \\
        }

        \tcp{find optimal split}
        \ForEach{bin}{
            calculate surface area heuristic cost for bin \\
            compare to current minimum \\
        }
    \end{algorithm}
    \caption{Pseudo code of split plane selection.}
    \label{alg:sah}
\end{wrapfigure}


%object median, spatial median, axis choice
A commonly used partitioning strategy is the object median which
simply subdivides the object set which has been ordered according to
a chosen axis by the median object and separates the two subsets. 
Similar to the object median is the spatial median partitioning 
strategy which subdivides the volume which the particles occupy 
and sorts the entire list into either the left or right child.

%SAH
The surface area heuristic(SAH) partitioning strategy is generally
considered to be the best algorithm for building trees. This 
recognition is based on empirical data it has simply been shown to 
produce better performing trees. The downside of using the SAH 
scheme is that it is considerably more complex to calculate than 
any of the simple schemes. By using the cost of the traversal of
a node structure compared to the cost of leaving it as a leaf node
the decision is made to subdivide the node. By calculating both the
cost if the node were made a leaf and the cost if it were subdivide
during the build time the decision is made. The SAH method is a 
local greedy algorithm which means that it does not globally 
optimize the cost, but only locally on a per node basis.

%implementation binned SAH
While the SAH method is the broad idea of how to decide where to 
split the particle list it is still not efficient enough to simply 
test all possible configurations. Therefore several adaptations 
have been published such as the binned SAH\cite{bvhbin} build 
algorithm which has been implemented as our build algorithm. Since 
the algorithm which evaluates the SAH cost of every split possible 
is to expensive to be performed an approximation in the form of 
regular splitting planes is used to evaluate and choose the most 
optimal partitioning. The binning method does not provide the most 
optimal subdivision possible for the set, instead it provides a 
subdivision which is considered sufficient to be used.

%walkthrough of sah binned algorithm
The pseudo code presented in figure \ref{alg:sah} shows the broad 
implementation without the messy details of actually implementing 
this algorithm. At first for each build item the largest axis is
found which will be used for any further calculations. While all
axes could quite easily be evaluated the partition cost is 
multiplied by three and therefore it was chosen that only using the
largest axis is sufficient. 

%sphere binning
Once the axis is chosen a set of bins is allocated which contain 
an axis aligned bounding box(AABB) and a counter to hold how many 
particles have their center in that particular bin. Each particle 
is projected into the bins by their bin id and the bin data is 
adjusted accordingly. The bin id is a calculation such that the 
center of the particle is assigned to one of the bins. The bounding
box of the particle is used to expand the bin bounding box to 
include the entire particle.

%aggregation sweep
Once binned particles are no longer required to evaluate which
means that only a single \(O(N)\) process is required for every
build item which is split. The aggregation sweeps are required to
calculate the basic variables which make up the SAH cost. The 
sweep from the left calculates the aggregated surface area of the
volume and the total number of spheres from that side, equally this
is done from the right hand side as well.

%sah evaluation
Finally once all bins have an associated total number of particles
and total surface area from both the lefthand and righthand sweep
the surface area cost for all bins can be calculated according to 
equation \ref{eg:sahcost}.

\begin{equation} \label{eg:sahcost}
    cost_i = traversalcost + leafcost\frac{A_{L,i}N_{L,i} + \\
        A_{R,i}N_{R,i}}{A_{parent}}
\end{equation}

When the least costing bin is found the algorithm separates the
particles into two groups by calculating the bin id of each particle
and placing it into a child item. In addition to the partition
selection the SAH method allows a very helpful termination criteria.
Since the traversal cost is calculated and used to determine where 
to split the object list it can also be used whether to split it
at all. By calculating the cost of splitting along side the cost of
simply leaving it as a leaf node which contains particles we are 
able to determine the most optimal structure.

\subsubsection{Tree Traversal}

%what is tree traversal
The traversal of the BVH is critical to a good performing tree
structure. When a ray is directed through the tree it is the
traversal method which decides which nodes to visit and which
ones to ignore. This decision is magnified by the number of
intersection tests that are performed when a node is determined to
be intersecting the ray.

%methods of decent, broadphase
The tree traversal is performed for every ray to reduce the number
of spheres to intersect. The traversal can be split into three 
sections broad phase, narrow phase and filtering. The broad phase 
search uses the previously built tree structure to find potentially
intersecting spheres for a given ray very quickly. By reducing the
number of sphere that are to be searched individually the time
taken for the entire tracing of the ray and calculating the column
density is reduced by several orders of magnitude. While the
broad phase search can result in spheres which are not actually
intersected by the ray it also guarantees that the spheres the
ray actually intersects are in this subset of the entire SPH 
dataset. The difficulty in this part of the algorithm is to perform
this broad search quickly, such that the time per ray is not spent
on a search through an additional structure which is not actually
required for solving the problem.

\begin{wrapfigure}{r}{0.5\textwidth}
    \begin{algorithm}[H]
        \KwIn{ray \& BVH}
        \KwData{stack of nodes to visit}
        \KwOut{column density of ray}

        push root node to stack

        \While{stack not empty}{
            pop node from stack
            \If{ray intersects node}{
                \eIf{node is a leaf}{
                    calculate column density of node
                    add to total column density of ray
                }{
                    push node children to stack
                }
            }
        }
    \end{algorithm}
    \caption{Pseudo code of BVH ray traversal.}
    \label{alg:traversal}

    \begin{algorithm}[H]
        \KwIn{ray \& BVH node}
        \KwOut{column density of ray through node}

        \ForEach{sphere in node}{
            \If{sphere is not intersected}{
                skip
            }

            \If{sphere intersection is filtered}{
                skip
            }

            calculate column density of sphere
        }
        
        multiply total column density by Gadget2 correction

    \end{algorithm}
    \caption{Pseudo code of column density calculation for 
             individual BVH node.}
    \label{alg:nodedensity}
\end{wrapfigure}

%ray-AABB intersection
The performance of the broad phase search depends mainly on the 
intersection routine which is used to intersect the axis aligned
bounding box(AABB) and the ray. Other secondary factors are the
memory layout of the actual nodes that compose the tree. In our
implementation the ray-AABB intersection algorithm\cite{raybox} 
used is highly optimized and generally considered the best method 
for this problem. Packets are intersected using a modified
\cite{pacbox} slab test method which is the same original method
\cite{slabtest} as the individual ray.

%narrow phase
The narrow phase of the ray tracing is the step in which the 
potential sphere intersection list is reduced to the actual
list of sphere which intersect the given ray. This is done by 
intersecting each sphere by the ray and therefore determining
whether an intersection it taking place and all the information for
further processing down the line to calculate the column density.

%ray-sphere intersection
The ray-sphere intersection mathematics is simply a quadratic 
equation which is solved to find both real roots if 
present\cite{rtcd}. By using a numerically stable 
algorithm\cite{numsis} the stability of these solutions is 
guaranteed. Optimizations are also done to reduce the computational
cost even more. The highest cost for the sphere intersection is the
square root function which must be called to calculate the 
parametric position of the intersections along the ray.

%ray filtering
Since rays can begin and end anywhere inside the simulation volume
it is necessary to deal with situations where rays do not entirely
intersect a sphere such that it enters and exits. Figure 
\ref{fig:filt} shows all situations which can arise with partial 
intersections of particles. The top and bottom solutions are both
simple when no intersection takes place or when a full intersection
takes place giving no contribution or a full contribution 
accordingly. The rays passing the center line are considered as
full contributing intersections while the rays which are on or
below the center line of the particle are ignored intersections.
This acts such that the particles are symmetrically over and under
estimated.

\begin{figure}[h]
    \centering

    \includegraphics[width=\textwidth]{graphics/filter}
    \caption{Ray-particle intersection cases.}
    \label{fig:filt}
\end{figure}

The filtering of the intersections is completed by comparing whether
the \(t_{min}\) or \(t_{max}\) are equal to the radius in which case
the center exclusion is present, otherwise if the negative of the
near intersection is greater than the far intersection the desired
behaviour is reached and used to exclude the rays which do not pass
the center line. This routine is executed once per ray-particle 
intersection therefore it should be very light weight and not 
require much resources to evaluate.

%implementation details
The implemented algorithm does not store large lists of spheres due
to memory consumption costs instead it executes these routines as
required on a per ray-particle intersection basis. Once a sphere has
been determined as contributing to the column density of that ray
the routine which calculates the column density of that particular
intersection is used.

\subsection{Column Density Calculation} \label{sec:cdc}
%kernel integration
The total column density of a ray is the sum of all particles
column densities which are intersected with the ray. To calculate
the value of the integral which is the column density of a particle
a numerical integration can be used to ensure flexibility between
different definitions of the kernel used in the SPH simulation. As
illustrated in figure \ref{fig:intk} the impact parameter \(b\) 
determines exactly how much of the particle is intersected by the
ray. Since the intersection points and the particle center form
a plane the entire problem can be constructed within a two 
dimensional space. In figure \ref{fig:intk} the values outside the
circle are zero due to the definition of the kernel presented in 
section \ref{sec:sph}. To calculate the total integral through the
kernel we must convert from a Cartesian coordinate system in which 
the problem is defined into a radial coordinate system for use of
the kernel function. Using a numerical integration scheme it is 
simple to calculate the total value of the integral along the ray.

\begin{figure}[t]
    \centering

    \includegraphics[width=\textwidth]{graphics/integration}
    \caption{Diagram of kernel integration.}
    \label{fig:intk}
\end{figure}

%look up table intro
Since performing an entire numerical integration each time a ray
intersects a particle is completely unfeasible it is required that
we pre calculate the values for the SPH kernel integration. 
A look up table(LUT) is constructed from the kernel integration
values at different impact parameters. By interpolating between
these stored values a quick solution to find the contribution of
each particle with every intersection is possible.

%impact parameter
To use a look up table an indexing method has to be devised
which is independent of the particles and only dependent on the 
SPH kernel function. The impact parameter is the distance from the
center of the particle to the closest point on the ray and it is
calculated for each intersection according to equation 
\ref{eq:para}.

\begin{equation}
    \textbf{b}_i = (\textbf{o} - \textbf{c}_i) + \\
                d(-((\textbf{o} - \textbf{c}_i)\cdot\textbf{d}))
\end{equation}

\begin{equation} \label{eq:para}
    b_i^2 = \textbf{b}_i \cdot \textbf{b}_i
\end{equation}

The impact parameter \(b\) is dependent on the specific 
intersection which took place, however when it is divided by the 
radius of the particle which was hit it becomes a quantity which 
describes the relative distance from the center of the particle to
the exterior. The quantity in equation \ref{eq:nb2} is the 
normalized squared impact parameter. The squared of the impact
parameter is used, because it allows the calculation to exclude a
square root operation which even on modern computer hardware is 
an expensive and time consuming operation, however since the it is
a squared space the transformation requires a higher amount of 
sampling of the integral compared to the normalized impact 
parameter.
\begin{equation} \label{eq:nb2}
    nb_i^2 = \frac{b_i^2}{h_i^2}
\end{equation}
The numerical value of the normalized squared impact parameter lies
between \(0.0\) and \(1.0\) since the maximum distance from the ray
to the particle center is the radius.

%look up table
The look up table is constructed by evaluating the integral shown
above at intervals of the normalized squared impact parameter. 
Once the look up table is used this allows with interpolation any 
SPH kernel integration value to be calculated with the normalized 
squared impact parameter. Linear interpolation was chosen as the 
interpolation method due to its simplicity, speed and lack of any 
boundary requirements compared to cubic spline interpolation which 
would require additional points outside the bounds for correctness
and more processing at run time. In addition the look up table was 
prepared in such a way to allow extremely fast indexing into it by 
using regular intervals of the normalized square impact parameter.

%interpolation search
An interpolation search on the prepared look up table is performed 
which allows a \(O(1)\) complexity for finding the indexes of the 
upper and lower bounds of the interpolation. Equation \ref{eq:low} 
and equation \ref{eq:high} show the calculation which is required 
for a single search. By simply flooring the normalized squared 
impact parameter the location in the look up table is already known.
This avoids both a linear search or a sorted binary search of 
complexity \(O(N)\) or \(O(\log{N}\) respectively.
\begin{equation} \label{eq:low}
    ilow = \lfloor nb_i^2 \rfloor
\end{equation}
\begin{equation} \label{eq:high}
    ihigh = ilow + 1
\end{equation}

%linear interpolation
When the indices of the SPH kernel values are found all that is
required is to calculate the linear interpolant between the two
contributing values, this is presented in equation \ref{eq:iplt}.
\begin{equation} \label{eq:iplt}
    iplt = \frac{nb_i^2 - nb2s[ilow]}{nb2s[ihigh]-nb2s[ilow]}
\end{equation}
\begin{equation} \label{eq:weight}
    value = weights[ilow](1.0-iplt) + weights[ihigh]iplt
\end{equation}
Finally with the linear interpolant the value at the normalized
squared impact parameter is calculated by simply weighting the
two contributions by the interpolant, this is the resulting weighing
due to the kernel from the particle intersection of the ray.
At this point the mass of all spheres weighted by the SPH kernel is
accumulated and can be used to calculate physical column densities.
