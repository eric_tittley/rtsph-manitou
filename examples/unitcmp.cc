#include <omp.h>

#include <fstream>

#include "Common.h"
#include "G2Dataset.h"
#include "BVH.h"
#include "Weight.h"
#include "Parser.h"
#include "Points.h"

#define sx 500
#define sy 500

void makeTree(const G2Dataset& data, BVH& tree) {
    //extract spheres from G2 data
    Spheres sphs; data.getSpheres(sphs);

    //build tree
    mkSAHBVH(sphs, tree);
}

int main(int argc, char** argv) {
    double start, end;
    
    std::cout << "parsing" << std::endl;
    G2Dataset data;
    std::string filename("aux/data/Data_025");
    start = omp_get_wtime();
    parseG2Dataset(filename, data);
    end = omp_get_wtime();
    std::cout << "parsed " << (end-start) << std::endl;

    //generate tree
    std::cout << "building" << std::endl;
    BVH tree;
    makeTree(data, tree);
    end = omp_get_wtime();
    std::cout << "built " << (end-start) << std::endl;

    //computation
    double* im = new double[sx*sy];

    //generate rays
    std::cout << "generate rays" << std::endl;
	Rays rays;
    for(uint32_t iy = 0; iy < sy; ++iy) {
        for(uint32_t ix = 0; ix < sx; ++ix) {
            //generate ray
            double px = (ix+0.5)/sx * 10000.0;
            double py = (iy+0.5)/sy * 10000.0;
            
            Vector3 p1, p2;
           
            //center
			p1.x() = px; p1.y() = py; p1.z() = -1000;
            p2.x() = px; p2.y() = py; p2.z() = 11000;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);

            //left
            p1.x() -= 10000.0;
            p2.x() -= 10000.0;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);

            //ltop
            p1.y() += 10000.0;
            p2.y() += 10000.0;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);

            //top
            p1.x() += 10000.0;
            p2.x() += 10000.0;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);

            //rtop
            p1.x() += 10000.0;
            p2.x() += 10000.0;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);

            //right
            p1.y() -= 10000.0;
            p2.y() -= 10000.0;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);

            //bright
            p1.y() -= 10000.0;
            p2.y() -= 10000.0;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);

            //bottom
            p1.x() -= 10000.0;
            p2.x() -= 10000.0;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);

            //lbottom
            p1.x() -= 10000.0;
            p2.x() -= 10000.0;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);
		}
    }

    //processing
    double dL = 10000.0/sx;
    double dA = dL*dL;

    double total = 0.0;

    start = omp_get_wtime();
    std::cout << "processing rays " << rays.count() << std::endl;
    #pragma omp parallel for schedule(static, 1)
    for(size_t iray = 0; iray < rays.count(); ++iray) {
        //fetch ray
        Ray ray; rays.get(iray, ray);

        //calculate column density
        double density = tree.weightRay(ray);
        density *= dA;

        //insert
        im[ray.id] += density;
        
        #pragma omp atomic
        total += density;
    }
    end = omp_get_wtime();
    std::cout << "processing done " << (end-start) << " s ";
    std::cout << std::endl;

    std::cout.precision(15);
    std::cout << "total - " << total << std::endl;

    std::cout << "outputing" << std::endl;
    start = omp_get_wtime();
    std::ofstream file("unit.cds");
    assert(file.good());
    uint64_t wx = sx, wy = sy;
    file.write((char*)&wx, sizeof(uint64_t));
    file.write((char*)&wy, sizeof(uint64_t));
    file.write((char*)im, sizeof(double)*sx*sy);
    file.close();
    end = omp_get_wtime();
    std::cout << "output " << (end-start) << std::endl;
}
