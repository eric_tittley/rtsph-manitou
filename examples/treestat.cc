#include <omp.h>

#include <fstream>

#include "Common.h"
#include "G2Dataset.h"
#include "BVH.h"
#include "Weight.h"
#include "Parser.h"
#include "Points.h"

void makeTree(const G2Dataset& data, BVH& tree) {
    //extract spheres from G2 data
    Spheres sphs; data.getSpheres(sphs);

    //build tree
    mkSAHBVH(sphs, tree);
}

int main(int argc, char** argv) {
    double start, end;
    
    std::cout << "parsing" << std::endl;
    G2Dataset data;
    std::string filename("aux/data/Data_025");
    start = omp_get_wtime();
    parseG2Dataset(filename, data);
    end = omp_get_wtime();
    std::cout << "parsed " << (end-start) << std::endl;

    //generate tree
    std::cout << "building" << std::endl;
    BVH tree;
    makeTree(data, tree);
    end = omp_get_wtime();
    std::cout << "built " << (end-start) << std::endl;

    //statistics
    std::stack<uint32_t> stack;
    stack.push(0);

    uint32_t size_min=1000, size_max=0, size_mean=0;
    uint32_t leafs = 0;

    while(!stack.empty()) {
        uint32_t inode = stack.top(); stack.pop();
        const BVHNode& node = tree.nodes[inode];

        //eval
        if(node.isLeaf()) {
            ++leafs;

            size_min = MIN(size_min, node.run);
            size_max = MAX(size_max, node.run);
            size_mean += node.run;
        }
        else {
            stack.push(node.right);
            stack.push(inode+1);
        }
    }

    //size_mean /= leafs;
    std::cout << "leafs " << leafs << std::endl;
    std::cout << "min " << size_min << std::endl;
    std::cout << "max " << size_max << std::endl;
    std::cout << "mean " << size_mean << std::endl;
}
