#! /usr/bin/python2

from struct import pack
import parse

h, p, r = parse.dataset("aux/data/Data_025")

bout = open('rad.lns', 'wb')

#write header
#unsigned long
bout.write(pack('L', len(r)))

#write data
#p1x, p1y, p1z, p2x, p2y, p2z, ... * count
for sph in range(0,len(r)):
    ps = p[sph]
    bout.write(pack('ddd', ps[0], ps[1], ps[2]))
    bout.write(pack('ddd', 5000.0, 5000.0, 5000.0))

bout.close()
