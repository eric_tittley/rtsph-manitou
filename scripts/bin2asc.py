#! /usr/bin/python

from __future__ import print_function

import numpy as np
from struct import unpack

fid = open("im.cds","rb")

nx = unpack("L", fid.read(8))[0]
ny = unpack("L", fid.read(8))[0]

ns = np.fromfile(fid, dtype='double', count=nx*ny)

np.savetxt("im.txt", ns)
