#pragma once
/// \file Spheres.h
/// \brief Sphere(SOA) structure

#include <vector>

#include "Common.h"
#include "Ray.h"
#include "Packet.h"

/// \brief Contains all geometry data required for ray tracing and integration.
///
///
struct Spheres {
    /// \brief Parallel array for x coordinate of spheres.
    std::vector<double> cx;

    /// \brief Parallel array for y coordinate of spheres.
    std::vector<double> cy;

    /// \brief Parallel array for z coordinate of spheres.
    std::vector<double> cz;

    /// \brief Parallel array for radius of spheres.
    ///todo remove this, not required
    std::vector<double> h;

    /// \brief Parallel array for inverse radius of spheres.
    std::vector<double> invh2;
    
    /// \brief Number of spheres.
    size_t count() const {
        return cx.size();
    }

    /// \brief Fetch Vector3 center for indexed sphere.
    void getCenter(size_t idx, Vector3& center) const {
        center.x() = (double)cx[idx];
        center.y() = (double)cy[idx];
        center.z() = (double)cz[idx];
    }

    /// \brief Intersect the indexed sphere with ray.
    ///
    ///
    //bool intersect(size_t, const Ray&, RayIntersect&) const;
    inline double intersect(size_t, const Ray&) const;

    /*#if defined __SIMD__
    ///
    ///
    ///
    void intersect(size_t, const Packet&, PacketIntersect&) const;
    #endif*/
};

double Spheres::intersect(size_t isph, const Ray& ray) const {
    //local space
    Vector3 local; getCenter(isph, local); 
    local -= ray.origin;
    double l2 = local.dot(local);

    //project onto direction
    double tb = ray.direction.dot(local);

    //pythagoras
    double tb2 = tb*tb;
    double b2 = l2 - tb2;

    //square normalized impact parameter
    double nb2 = b2 * invh2[isph];

    //impose roots & ray limits & filtering
    //1.0 will contribute 0.0 as kernel value
    return (nb2<=1.0) && (0.0<tb) && (tb<ray.tmax) ? nb2 : 1.0;
}
