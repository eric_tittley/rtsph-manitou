#pragma once
/// \file AABB.h
/// \brief Contains Smits Axis Aligned Bounding Box implementation.

#include "Common.h"
#include "Vector3.h"
#include "Ray.h"
#include "Packet.h"

///
///
///
struct AABB {
//TODO breaks building, if we switch to private
//private:
    Vector3 bounds[2];

public:
    //TODO convert
    //double nx, ny, nz;
    //double xx, xy, xz;
    
    ///
    ///
    ///
    AABB() {
        bounds[0] = Vector3( real_inf);
        bounds[1] = Vector3(-real_inf);

        //nx = ny = nz =  real_inf;
        //xx = xy = xz = -real_inf;
    }
    
    ///
    ///
    ///
    void expand(const Vector3& pnt) {
        bounds[0].min(pnt);
        bounds[1].max(pnt);

        //nx = MIN(nx, pnt.x(); ny = MIN(ny, pnt.y(); nz = MIN(nz, pnt.z());
        //xx = MAX(xx, pnt.x(); xy = MAX(xy, pnt.y(); xz = MAX(xz, pnt.z());
    }
    
    ///
    ///
    ///
    void expand(const AABB& aabb) {
        //max should only be able to increase max
        //min should only be able to increase min
        bounds[0].min(aabb.bounds[0]);
        bounds[1].max(aabb.bounds[1]);

        //nx = MIN(nx, aabb.nx); ny = MIN(ny, aabb.ny); nz = MIN(nz, aabb.nz);
        //xx = MAX(xx, aabb.xx); xy = MAX(xy, aabb.xy); xz = MAX(xz, aabb.xz);
    }

    /// \brief Calculates the surface area for Surface Area Heuristic of the AABB.
    ///
    ///
    double surfacearea() const {
        Vector3 dv;
        dv += bounds[1];
        dv -= bounds[0];

        //double dx = xx - nx, dy = xy - ny, dz = xz - nz;
        //double half = dx*dy + dx*dz + dy*dz;

        double sa = (dv[0] * dv[1]) 
                  + (dv[0] * dv[2]) 
                  + (dv[1] * dv[2]);

        if(sa < 0.0) 
            return 0.0;
        else
            return 2.0 * sa;
    }

    /// \brief Resets the AABB to the original.
    ///
    ///
    void clear() {
        bounds[0].set( real_inf);
        bounds[1].set(-real_inf);

        //nx = ny = nz =  real_inf;
        //xx = xy = xz = -real_inf;
    };

    /// \brief Smit Ray-AABB intersection algorithm.
    ///
    ///
    inline bool intersect(const Ray& ray) const;

    #if defined __SIMD__
    ///
    ///
    ///
    inline bool intersect(const Packet& packet) const;
    #endif
};

bool AABB::intersect(const Ray& ray) const {
    double t0, t1;
    double tymin, tymax, tzmin, tzmax;

    //todo replace
    //double divx = 1.0 / ray.direction.x();
    double divx = ray.invdir.x();
    if(divx >= 0) {
        //t0 = (bounds[0].x() - ray.origin.x()) * divx;
        t0 = (bounds[0].x()*divx) - (ray.origin.x()*divx);
        //t1 = (bounds[1].x() - ray.origin.x()) * divx;
        t1 = (bounds[1].x()*divx) - (ray.origin.x()*divx);
    }
    else {
        t0 = (bounds[1].x() - ray.origin.x()) * divx;
        t1 = (bounds[0].x() - ray.origin.x()) * divx;
    }

    //t0 = (bounds[ray.xmin].x()*divx) - (ray.origin.x()*divx);
    //t1 = (bounds[ray.xmax].x()*divx) - (ray.origin.x()*divx);

    //todo replace
    //double divy = 1.0 / ray.direction.y();
    double divy = ray.invdir.y();
    if(divy >= 0) {
        tymin = (bounds[0].y() - ray.origin.y()) * divy;
        tymax = (bounds[1].y() - ray.origin.y()) * divy;
    }
    else {
        tymin = (bounds[1].y() - ray.origin.y()) * divy;
        tymax = (bounds[0].y() - ray.origin.y()) * divy;
    }

    //early exit 1
    if((t0 > tymax) || (tymin > t1))
        return false;

    t0 = MAX(t0, tymin);
    t1 = MIN(t1, tymax);

    //todo replace
    //double divz = 1.0 / ray.direction.z();
    double divz = ray.invdir.z();
    if(divz >= 0) {
        tzmin = (bounds[0].z() - ray.origin.z()) * divz;
        tzmax = (bounds[1].z() - ray.origin.z()) * divz;
    }
    else {
        tzmin = (bounds[1].z() - ray.origin.z()) * divz;
        tzmax = (bounds[0].z() - ray.origin.z()) * divz;
    }
    
    //early exit 2
    if((t0 > tzmax) || (tzmin > t1))
        return false;

    t0 = MAX(t0, tzmin);
    t1 = MIN(t1, tzmax);

    //ray limits
    return (t0 <= ray.tmax) && (0.0 <= t1);
}

#if defined __SIMD__
bool AABB::intersect(const Packet& packet) const {
    //axis-x slab
    doublew bminx = set1pd(bounds[0].x());
    doublew bmaxx = set1pd(bounds[1].x());
    doublew bminy = set1pd(bounds[0].y());
    doublew bmaxy = set1pd(bounds[1].y());
    doublew bminz = set1pd(bounds[0].z());
    doublew bmaxz = set1pd(bounds[1].z());
    doublew any = falsew;

    for(size_t i = 0; i < PACKET_SIZE; ++i) {
        //doublew l1x = divpd(subpd(bminx, packet.ox[i]), packet.dx[i]);
        //doublew l2x = divpd(subpd(bmaxx, packet.ox[i]), packet.dx[i]);
        doublew l1x = mulpd(subpd(bminx, packet.ox[i]), packet.invdx[i]);
        doublew l2x = mulpd(subpd(bmaxx, packet.ox[i]), packet.invdx[i]);

        doublew lmin = minpd(l1x, l2x);
        doublew lmax = maxpd(l1x, l2x);

        //doublew l1y = divpd(subpd(bminy, packet.oy[i]), packet.dy[i]);
        //doublew l2y = divpd(subpd(bmaxy, packet.oy[i]), packet.dy[i]);
        doublew l1y = mulpd(subpd(bminy, packet.oy[i]), packet.invdy[i]);
        doublew l2y = mulpd(subpd(bmaxy, packet.oy[i]), packet.invdy[i]);

        lmin = maxpd(minpd(l1y, l2y), lmin);
        lmax = minpd(maxpd(l1y, l2y), lmax);
    
        //doublew l1z = divpd(subpd(bminz, packet.oz[i]), packet.dz[i]);
        //doublew l2z = divpd(subpd(bmaxz, packet.oz[i]), packet.dz[i]);
        doublew l1z = mulpd(subpd(bminz, packet.oz[i]), packet.invdz[i]);
        doublew l2z = mulpd(subpd(bmaxz, packet.oz[i]), packet.invdz[i]);

        lmin = maxpd(minpd(l1z, l2z), lmin);
        lmax = minpd(maxpd(l1z, l2z), lmax);   

        //any intersections
        doublew omask = cmpltpd(lmin, lmax);

        //ray limit mask
        doublew xmask = cmplepd(lmin, packet.tmax[i]);
        //doublew nmask = cmplepd(packet.tmin[i], lmax);
        doublew nmask = cmplepd(zero, lmax);
        
        doublew fmask = andpd(omask, andpd(xmask, nmask));
    
        //any filtered
        any = orpd(any, fmask);
    }
    return movemaskpd(any) > 0;
}
#endif
