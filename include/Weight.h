#pragma once
/// \file Weight.h
/// \brief Includes all look up table related functions and data.
///
/// This file contains the prepared look up table of the integral
/// of the Gadget2 SPH Kernel and functions to calculate the weight
/// contributed by a sphere which is intersected by a ray.

#include "Common.h"
#include "SIMD.h"
#include "Packet.h"
#include "Ray.h"

//---------LINEAR_SPACE---------
/*const double nb2s[21] ALIGN16 = {
    0.0, 0.05,0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 
    0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0
};

const double weights[21] ALIGN16 = {
    1.90985931710274, 1.88367053830498, 1.80775802960417,
    1.68846204942205, 1.53427342301935, 1.35501359909301,
    1.16121224032458, 0.963552588990662, 0.772314581594136,
    0.59673924045607, 0.444144243871787, 0.3180318576118,
      0.21753864865246, 0.140537291782395, 0.0843208762752769,
    0.0457692678567761, 0.0215055224403573, 0.00805145605612982,
   0.00199372958339108, 0.000180185095125343, 0
};

const double dwdnb[21] ALIGN16 = {
    0, -1.03748524737601, -1.97745677609773, -2.76582005503512,
    -3.36878343945246, -3.76632793205878, -3.9499328169823,
    -3.92198067918625, -3.69646429796699, -3.30165218631495,
    -2.78899856334974, -2.26010819586592, -1.76677270139369,
    -1.32235969573472, -0.936749178377869, -0.616623081981238,
    -0.365575314578304, -0.184028801065274, -0.0688736832421891,
    -0.0125346196634912, 0
};*/

//---------QUADRATIC_SPACE---------
const double nb2s[26] ALIGN16 = {
    0, 0.04, 0.08, 0.12, 0.16, 0.20, 0.24, 0.28, 0.32, 0.36, 0.4, 
    0.44, 0.48, 0.52, 0.56, 0.6, 0.64, 0.68, 0.72, 0.76, 0.8, 
    0.84, 0.88, 0.92, 0.96, 1.0
};

const double weights[26] ALIGN16 = {
    1.90985931710274, 1.53427342301935, 1.22868539826899,
    0.977648031761343, 0.772314581594136, 0.605974922594714,
    0.472870497246736, 0.367408130803053, 0.283835318803926,
    0.21753864865246, 0.165040055726239, 0.12364681453937,
    0.0912303865983847, 0.0660806554273892, 0.0468060989689934,
    0.0322632466848459, 0.0215055224403573, 0.0137453413566596,
    0.00832553083795483, 0.004697487654777, 0.00240433400643329,
    0.00106790193021249, 0.000378784345432041, 8.90458550752027e-05,
    7.65452292466834e-06, 0.0
};

const double dwdnb[26] ALIGN16 = {
    -10.50564735208475,
    -8.514673985421874,
    -6.957817390725087,
    -5.7046352084356755,
    -4.645913864582862,
    -3.7430510543424997,
    -2.982084897395762,
    -2.3629397305351256,
    -1.8733685268824125,
    -1.4849407884710872,
    -1.173647926413625,
    -0.9226208640981788,
    -0.7195769888997601,
    -0.5553035953673913,
    -0.4227176092817912,
    -0.3162572066079512,
    -0.23147381660232874,
    -0.16474989503003085,
    -0.1130981712735325,
    -0.07201496039401925,
    -0.04336982155705638,
    -0.024235700939216089, 
    -0.0116391227813421575,    
    -0.003930731884400338,
    -0.0005813630731167085,
     0.0
};

/// \brief Lookup table for impact parameter values. 
///
/// This is mostly for understandablility of the code. The table is
/// never actually used, because it was optimized out by using a 
/// uniform difference across the LUT.

/// \brief The lookup table values for the weights.
///
/// The look up table contains the kernel intergrals calculated
/// from the Gadget2 SPH Kernel using MATLAB. The values are
/// interpolated by an optimized linear interpolator.

/// Number of elements in the look up table minus 1. Used for 
/// optimization of the interpolation search.
const double range = 25.0 - bepsilon;
/// The LUT is prepared to have equal distance between samples of
/// the function, therefore a single inverse is used to scale the
/// interpolant instead of a memory access into the nbs array.
const double df = 0.04; 
const double invdf = 25.0; 

//Prototypes
inline double kernelnb(double nb);
inline double kernelnb2(double nb2);
inline void searchnb2(double nb2, size_t& ilow, size_t& ihigh);
inline double linearnb2(double nb2);
inline double cubicnb2(double nb2);

//Definitions
inline double kernelnb(double nb) {
    //0.0 <= nb <= 1.0
    return kernelnb2(nb*nb);
}

inline double kernelnb2(double nb2) {
    //0.0 <= nb2 <= 1.0
    return cubicnb2(nb2); 
}

inline void searchnb2(double nb2, size_t& ilow, size_t& ihigh) {
    //interpolation search
    //size_t ilow = (size_t)floor(nb2 * range);

    ilow = (size_t)(nb2 * range);
    ihigh = ilow + 1;
}

inline double linearnb2(double nb2) {
    //interpolation bounds search
    size_t ilow, ihigh; searchnb2(nb2, ilow, ihigh);

    //linear interpolation
    double intplt = (nb2-nb2s[ilow]) * invdf;

    double lcontrib = weights[ilow]  * (1.0 - intplt);
    double hcontrib = weights[ihigh] *        intplt;

    return lcontrib + hcontrib;
}

inline double cubicnb2(double nb2) {
    //interpolation bounds search
    size_t ilow, ihigh; searchnb2(nb2, ilow, ihigh);

    //cubic hermite interpolation in quadratic space
    double t = (nb2 - nb2s[ilow]) * invdf;
    double t2 = t*t;
    double t3 = t*t2;

    double h00 = (2*t3) - (3*t2) + 1;
    double h10 = (t3 - (2*t2) + t) * df;
    double h01 = (-2*t3) + (3*t2);
    double h11 = (t3 - t2) * df;

    double pl = weights[ilow] * h00;
    double ml = dwdnb[ilow] * h10;

    double ph = weights[ihigh] * h01;
    double mh = dwdnb[ihigh] * h11;

    double returnValue = (pl + ph) + (ml + mh);
    
    if(returnValue<__DBL_MIN__) returnValue=0.0;

    return returnValue;
}

/*#if defined __SIMD__
///
///
///
const doublew rangew = set1pd(range);

///
///
///
const doublew invdfw = set1pd(invdf);

///
///
///
inline void kernel(const doublew nb2[PACKET_SIZE], doublew kern[PACKET_SIZE]) {
    //interpolation search
    //doublew lowsd = floorpd(mulpd(nb, range));
    int4 ilowsw[PACKET_SIZE];
    for(size_t i = 0; i < PACKET_SIZE; ++i) {
        doublew snb = mulpd(nb2[i], rangew);
        ilowsw[i] = cvttpd_epi32(snb);
    }

    //read out ints
    //TODO check whether works with both SSE and AVX
    int ilows[PACKET_COUNT] ALIGN16;
    for(size_t i = 0; i < PACKET_SIZE; ++i) {
        storesi128((__m128i*)&ilows[i*4], ilowsw[i]);
    }

    //serial load weights
    double nb2slow[PACKET_COUNT] ALIGN16;
    double wslow[PACKET_COUNT] ALIGN16;
    double wshigh[PACKET_COUNT] ALIGN16;
    for(size_t i = 0; i < PACKET_COUNT; ++i) {
        assert(0 <= ilows[i]);
        assert(ilows[i] < 51);

        nb2slow[i] = nb2s[ilows[i]];
        wslow[i] = weights[ilows[i]];
        wshigh[i] = weights[ilows[i]+1];
    }

    //load into doublew
    doublew wnb2slow[PACKET_SIZE];
    doublew wwslow[PACKET_SIZE], wwshigh[PACKET_SIZE];
    for(size_t i = 0; i < PACKET_SIZE; ++i) {
        wnb2slow[i] = loadpd(&nb2slow[i*__WIDTH__]);
        wwslow[i] = loadpd(&wslow[i*__WIDTH__]);
        wwshigh[i] = loadpd(&wshigh[i*__WIDTH__]);
    }

    //interpolants
    doublew iplts[PACKET_SIZE], nplts[PACKET_SIZE];
    for(size_t i = 0; i < PACKET_SIZE; ++i) {
        doublew lplt = subpd(nb2[i], wnb2slow[i]);
        
        iplts[i] = mulpd(lplt, invdfw);
        nplts[i] = subpd(one, iplts[i]);
    }
   
    //interpolation
    for(size_t i = 0; i < PACKET_SIZE; ++i) {
        doublew wf = mulpd(wwslow[i], nplts[i]);
        doublew ws = mulpd(wwshigh[i], iplts[i]);

        kern[i] = addpd(wf, ws);
    }
}
#endif*/
