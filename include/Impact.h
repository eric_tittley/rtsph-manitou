#pragma once
/// \file Impact.h
/// \brief Impact parameter related math

#include "Common.h"
#include "Vector3.h"

/// \brief Calculates normalized impact parameter of Ray-Sphere.
///
///
/*inline double nimpact(const Ray& ray, const RayIntersect& intersect, size_t idx, const Spheres& spheres) {
    double tp = (intersect.t0 + intersect.t1) * 0.5;

    Vector3 bv = ray.direction; bv *= tp; bv += ray.origin;
    Vector3 sc; spheres.getCenter(idx, sc);
    bv -= sc;

    double b2 = bv.length2();

    //normalized impact parameter
    return b2 / (double)spheres.r2[idx];
}*/

/*#if defined __SIMD__
///
///
///
inline void nimpact(const Packet& packet, const PacketIntersect& intrst, size_t idx, const Spheres& spheres, doublew nb2[PACKET_SIZE]) {
    doublew cxw = set1pd(spheres.cx[idx]);
    doublew cyw = set1pd(spheres.cy[idx]);
    doublew czw = set1pd(spheres.cz[idx]);
    doublew invr2w = set1pd(1.0/spheres.r2[idx]);

    for(size_t i = 0; i < PACKET_SIZE; ++i) {

        //calculate projected parametric
        doublew tp = mulpd(addpd(intrst.t0[i], intrst.t1[i]), half);

        //calculate impact vector bv
        doublew bvx = addpd(mulpd(packet.dx[i], tp), packet.ox[i]);
        doublew bvy = addpd(mulpd(packet.dy[i], tp), packet.oy[i]);
        doublew bvz = addpd(mulpd(packet.dz[i], tp), packet.oz[i]);

        //calculate (bv - sc)
        doublew bsx = subpd(bvx, cxw);
        doublew bsy = subpd(bvy, cyw);
        doublew bsz = subpd(bvz, czw);

        //length2 of (bv - sc)
        doublew bsx2 = mulpd(bsx, bsx);
        doublew bsy2 = mulpd(bsy, bsy);
        doublew bsz2 = mulpd(bsz, bsz);
        doublew b2 = addpd(bsx2, addpd(bsy2, bsz2));
        
        //normalize
        nb2[i] = mulpd(b2,invr2w);
    }
}
#endif*/
