#include "Weight.h"
#include <iostream>

#define SIZE 10000

int main() {
    double radius = 100.0;

    double radius2 = radius*radius;
    double diameter = radius*2.0;
    double dL = diameter/SIZE;
    double dA = dL*dL;

    //integrate over entire sphere
    double total = 0.0;

    for(size_t iy = 0; iy < SIZE; ++iy) {
        for(size_t ix = 0; ix < SIZE; ++ix) {
            double dy = -radius + diameter * (iy+0.5)/SIZE;
            double dx = -radius + diameter * (ix+0.5)/SIZE;

            double b2 = dx*dx + dy*dy;
            double nb2 = b2 / radius2;

            //sphere intersection
            if((0.0 <= nb2) && (nb2 <= 1.0)) {
                double k2d = kernelnb2(nb2);
                total += k2d;
            }
        }
    }

    //integral
    total *= dA;
    //divide by h^-2
    total /= radius2;

    printf("Total=%f\n", total);
    //std::cout << total << std::endl;
}
