#include <string>
#include <cassert>

#include <fstream>

#include <omp.h>

#include "BVH.h"
#include "Kernel.h"
#include "Parser.h"

#define Y 0.274
#define XHI 1.0
#define mH 1.66e-27
#define invmH 6.02214129e26
#define XHImY (XHI*(1.0-Y))

#define RAYS 50000

int main(int argc, char** argv) {
    double start, end;

    //parse dataset
    //spheres_t spheres;
    Sphere *spheres = 0;
    uint32_t count = 0;
    real_t gmass = 0.0;
    start = omp_get_wtime();
    parse("aux/data/Data_025", spheres, count, gmass);
    end = omp_get_wtime();
    printf("parse %f\n", end-start);

    //generate tree
    BVH tree;
    start = omp_get_wtime();
    mkSAHBVH(spheres, count, tree);
    end = omp_get_wtime();
    printf("build %f\n", end-start);
    //delete[] spheres;

    //analysis of tree
    /*double avgrun = 0;
    uint32_t maxrun = 0, minrun = 100000;
    for(uint32_t idx = 0; idx < tree.leafs.size(); ++idx) {
        maxrun = maxrun < tree.leafs[idx].run ? tree.leafs[idx].run : maxrun;
        minrun = minrun < tree.leafs[idx].run ? minrun : tree.leafs[idx].run;
        avgrun += tree.leafs[idx].run;
    }
    avgrun /= tree.leafs.size();
    printf("avg run %f | max run %u | min run %u\n", avgrun, maxrun, minrun);*/

    //generate rays
    srand(5343);
    SmitAABB bbox = tree.nodes[0].bbox;
    Vector3 center; 
    center += bbox.bounds[0]; 
    center += bbox.bounds[1];
    center *= 0.5;

    //AoS hot/cold not SoA, don't touch frequently
    Ray* rays = new Ray[RAYS];
    for(uint32_t iray = 0; iray < RAYS; ++iray) {
        rays[iray].origin = center;
        rays[iray].direction = randVector3().normalized();
        rays[iray].tmax = 10000;
        //rays[iray].initialize();
    }

    //computation
    real_t ncol[RAYS] = {0.0};

    leafs_t leafs;
    leafs.reserve(500);
    
    real_t ttpr = 0.0;
    for(uint32_t iray = 0; iray < RAYS; ++iray) {
        start = omp_get_wtime();
        const Ray& ray = rays[iray];
        //Ray2 ray2(ray);

        //broadphase
        tree.traverse(ray, leafs);
        //TODO find statistics, how much of the tree is touched

        //narrowphase
        for(uint32_t ileaf = 0; ileaf < leafs.size(); ++ileaf) {
            //uint32_t lref = leafs[ileaf];
            //const BVHLeaf& leaf = tree.leafs[lref];
            BVHLeaf lf = leafs[ileaf];

            //TODO prefetch all spheres into cache

            //iterate spheres in leaf
            for(uint32_t inc = 0; inc < lf.run; ++inc) {
                uint32_t idx = lf.offset + inc;

                //intersect
                real_t t0, t1;
                if(!tree.spheres.intersect(idx, ray, t0, t1)) continue;

                //filter
                if(t0 == tree.spheres.r[idx] || t1 == tree.spheres.r[idx]) continue; //flag
                else if(-t0 > t1) continue;

                //calculate boh
                real_t tp = (t0 + t1) * 0.5;
                Vector3 bv;
                ray(tp, bv);
                bv.com[0] -= tree.spheres.cx[idx];
                bv.com[1] -= tree.spheres.cy[idx];
                bv.com[2] -= tree.spheres.cz[idx];
                
                real_t nb = bv.length() * tree.spheres.ir[idx];
                //real_t nb2 = bv.length2() * tree.spheres.ir2[idx];
                
                //calculate weight
                real_t weight = kernel(nb);
                //real_t weight = kernel(nb2);
                weight *= (tree.spheres.ir2[idx]);

                //calculate physics
                real_t ncoli = XHImY * gmass * weight;
                ncoli *= invmH;
                ncol[iray] += ncoli;
            }
        }
    
        leafs.clear();

        end = omp_get_wtime();
        ttpr += (end-start);
    }

    printf("ttpr %f\n", ttpr/RAYS);

    std::ofstream file("columns.dat");
    for(size_t idx = 0; idx < RAYS; ++idx) {
        file << idx << " " << ncol[idx] << std::endl;
    }
    file.close();
}
