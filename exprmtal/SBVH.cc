#include "SBVH.h"

#include <algorithm>
#include <stack>

void SBVH::traverse(ray_ct& ray, sphints_t&) const {
    //sort and prune
    //compressed matrix format for mailbox!!!
}

void SBVH::print() const {
    std::stack<uint32_t> stack;
    std::stack<uint32_t> dstack;

    stack.push(0); dstack.push(0);

    while(!stack.empty()) {
        //retrieve node data
        uint32_t inode = stack.top(); stack.pop();
        uint32_t depth = dstack.top(); dstack.pop();
        const SBVHNode& node = nodes[inode];

        //output depth
        for(uint32_t d = 0; d < depth; ++d)
            std::cout << "-";
        std::cout.width(2); std::cout << depth;

        //output node
        if(node.leaf) {
            std::cout << "L";
            std::cout << node.off << ":" << node.run;
        }
        else {
            std::cout << "I";
            
            //push children to stack
            stack.push(node.right); dstack.push(depth+1);
            stack.push(node.left); dstack.push(depth+1);
        }

        std::cout << std::endl;
        std::cout.width(1);
    }
}

void SBVH::broadphase(ray_ct& ray, sphrefs_t& ptl) const {
    //initiate stack with root
    std::stack<uint32_t> stack;
    stack.push(0);

    while(!stack.empty()) {
        //retrieve sbvh node
        uint32_t inode = stack.top();
        const SBVHNode& node = nodes[inode];
        stack.pop();

        //check if ray hits node
        real_t nt0, nt1;
        if(node.bbox.intersect(ray, nt0, nt1)) {
            //process leaf node
            if(node.leaf) {
                //add all references to potential list
                for(uint32_t inc = 0; inc < node.run; ++inc) {
                    //lookup reference
                    uint32_t idx = node.off + inc;
                    sphref_t ref = refs[idx];

                    //add reference to potential list
                    ptl.push_back(ref);
                }
            }
            //process internal node
            else {
                //walk down tree
                //XXX push the one that is closer to ray origin
                stack.push(node.right);
                stack.push(node.left);
            }
        }
    }
}
