#pragma once

#include <vector>

#include "Common.h"
#include "Vector3.h"

#include "Ray.h"
#include "RPacket.h"

/*struct Sphere {
    Vector3 c;
    double r;

    Sphere() {}
    Sphere(const Vector3& center, double radius)
    : c(center), r(radius) {}

    //double r2, ir, ir2;
    //double pad;
    
    //void initialize() {
    //    r2 = r*r;
    //    ir = 1.0 / r;
    //    ir2 = 1.0 / r2;
    //}

    //bool intersect(const Ray& ray, double t[2]) const {
    bool intersect(const Ray& ray, double& t0, double& t1) const {
        //translate to obect space
        Vector3 tr;
        tr += ray.origin;
        tr -= c;

        //quadratic coefficents
        double b = ray.direction.dot(tr);
        //real_t c = tr.dot(tr) - r2;
        double c = tr.dot(tr) - (r*r);

        //quadratic discriminant
        double disc = (b*b) - c;
        if(disc < 0.0) return false;

        //quadratic formula
        double sdisc = sqrt(disc);

        //t solutions
        t0 = -b - sdisc;
        t1 = -b + sdisc;

        //any intersecting points
        return (t0 <= ray.tmax) && (ray.tmin <= t1);
    }

    //void intersect(const RPacket& packet, double4& alive, double4& t[2]) const {
    void intersect(const RPacket& packet, double4& active, double4& t0, double4& t1) const {
        //SIMD sphere
        double4 cx4, cy4, cz4, r4, r24;
        cx4 = _mm256_set1_pd(c.com[0]);
        cy4 = _mm256_set1_pd(c.com[1]);
        cz4 = _mm256_set1_pd(c.com[2]);
        r4 = _mm256_set1_pd(r);
        r24 = _mm256_mul_pd(r4,r4);

        //transform to local
        double4 lx, ly, lz;
        lx = _mm256_sub_pd(packet.ox, cx4);
        ly = _mm256_sub_pd(packet.oy, cy4);
        lz = _mm256_sub_pd(packet.oz, cz4);

        //B
        double4 qbx = _mm256_mul_pd(lx, packet.dx);
        double4 qby = _mm256_mul_pd(ly, packet.dy);
        double4 qbz = _mm256_mul_pd(lz, packet.dz);
        double4 qb = _mm256_add_pd(_mm256_add_pd(qbx, qby), qbz);

        //C
        double4 qcx = _mm256_mul_pd(lx,lx);
        double4 qcy = _mm256_mul_pd(ly,ly);
        double4 qcz = _mm256_mul_pd(lz,lz);
        double4 qcxy = _mm256_add_pd(qcx, qcy);
        double4 qczr = _mm256_sub_pd(qcz, r24);
        double4 qc = _mm256_add_pd(qcxy, qczr);

        //discriminant
        double4 qb2 = _mm256_mul_pd(qb, qb);
        double4 disc = _mm256_sub_pd(qb2, qc);
        double4 sdisc = _mm256_sqrt_pd(disc);

        //output
        active = _mm256_cmp_pd(disc, zero, _CMP_GE_OQ);
        t0 = _mm256_sub_pd(zero, _mm256_add_pd(qb, sdisc));
        t1 = _mm256_sub_pd(sdisc, qb);

        //eval ray limits
        active = _mm256_and_pd(active, 
                 _mm256_cmp_pd(t0, packet.tmax, _CMP_LE_OQ));
        active = _mm256_and_pd(active, 
                 _mm256_cmp_pd(packet.tmin, t1, _CMP_LE_OQ));
    }
};

typedef Sphere sphere_t;
typedef const sphere_t sphere_ct;

typedef std::vector<Sphere> spheres_t;
typedef const spheres_t spheres_ct;

typedef uint32_t sphref_t;
typedef std::vector<sphref_t> sphrefs_t;
typedef const sphrefs_t sphrefs_ct;*/
