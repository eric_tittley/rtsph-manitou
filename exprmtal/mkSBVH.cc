#include "SBVH.h"
#include <cassert>
#include <stack>
#include <limits>

//SAH parameters
#define BINS 25
#define epsilon 1e-6
#define real_inf std::numeric_limits<real_t>::infinity()

//internal data
struct ObjectSplit {
    uint32_t bin, axis;
    real_t cost;
};

struct SpatialSplit {
    uint32_t bin, axis;
    real_t cost;
};

struct SBVHItem {
    SmitAABB cb, bb;
    uint32_t off, run, pnode;
    bool right, root;
};

//internal prototypes
ObjectSplit evalSAH(const SBVHItem&, const SBVH&);
void partitionSAH(const ObjectSplit&, const SBVHItem&, SBVH&, SBVHItem&, SBVHItem&);

SpatialSplit evalChop(const SBVHItem&, const SBVH&);

uint32_t binID(const SBVHItem&, const ObjectSplit&, const Vector3&);
void centroidBB(const SBVH&, SBVHItem&);
void boundingBox(const SBVHItem&, const SBVH&, SBVHNode&);

//definitions
void mkSBVH(spheres_ct& spheres, SBVH& sbvh) {
    //check input
    assert(spheres.size() > 0);

    //reference spheres
    sbvh.sphs = &spheres;
    for(uint32_t i = 0; i < sbvh.sphs->size(); ++i) {
        sbvh.refs.push_back(i);
    }

    //construct SAH BVH
    std::stack<SBVHItem> stack;
   
    //insert root item
    SBVHItem rtitem;
    rtitem.off = 0; rtitem.run = sbvh.refs.size();

    for(uint32_t inc = 0; inc < rtitem.run; ++inc) {
        uint32_t idx = rtitem.off + inc;
        uint32_t ref = sbvh.refs[idx];
        const Sphere& sph = (*sbvh.sphs)[ref];
        Vector3 r3(sph.radius, sph.radius, sph.radius);
      
        rtitem.cb.expand(sph.center);
        rtitem.bb.expand(sph.center - r3);
        rtitem.bb.expand(sph.center + r3);
    }

    rtitem.right = false; rtitem.root = true;
    stack.push(rtitem);

    //build all items
    while(!stack.empty()) {
        //get build item
        SBVHItem item = stack.top(); stack.pop();

        //create child
        uint32_t inode = sbvh.nodes.size();
        SBVHNode node;
        
        //set parent indices
        if(!item.root) {
            if(item.right)
                sbvh.nodes[item.pnode].right = inode;
            else
                sbvh.nodes[item.pnode].left = inode;
        }

        //calculate split cost & split position
        ObjectSplit osplit = evalSAH(item, sbvh);
        SpatialSplit ssplit = evalChop(item, sbvh);
        
        //calculate leaf cost
        double lcost = item.run * C_LEAF;

        //if((lcost < osplit.cost) && (lcost < ssplit.cost)) {

        if(osplit.cost > ssplit.cost) {
            std::cerr << "c " << lcost << " " << osplit.cost << " ";
            std::cerr << ssplit.cost << std::endl;
        }

        if(lcost < osplit.cost) {
            node.leaf = true;

            node.off = item.off;
            node.run = item.run;

            sbvh.nodes.push_back(node);
            continue;
        }
         
        //create left & right items
        SBVHItem litem, ritem;
        litem.root = false; litem.pnode =inode; litem.right = false;
        ritem.root = false; ritem.pnode = inode; ritem.right = true;

        node.leaf = false;
        /*if(ssplit.cost < osplit.cost) {
            //spatial split
            assert(false);
        }
        else {*/
            //object split
            //apply partition
            partitionSAH(osplit, item, sbvh, litem, ritem);

            //push to build item
            stack.push(ritem); stack.push(litem);
        //}*/

        //write node
        sbvh.nodes.push_back(node);
    }
}

uint32_t binID(const SBVHItem& item, const ObjectSplit& split, const Vector3& vec) {
    assert(vec[split.axis] >= item.cb.min[split.axis]);
    assert(vec[split.axis] <= item.cb.max[split.axis]);
    
    real_t top = vec[split.axis] - item.cb.min[split.axis];
    real_t bot = item.cb.max[split.axis] - item.cb.min[split.axis];
    return BINS * (1.0 - epsilon) * top / bot;
}

void centroidBB(const SBVH& sbvh, SBVHItem& item) {
    item.cb.clear();

    for(uint32_t inc = 0; inc < item.run; ++inc) {
        uint32_t idx = item.off + inc;
        uint32_t ref = sbvh.refs[idx];
        const Sphere& sph = (*sbvh.sphs)[ref];
        
        item.cb.expand(sph.center);
    }

    //item.invcbs.inv(item.cb.max - item.cb.min);
}

void boundingBox(const SBVHItem& item, const SBVH& sbvh, SBVHNode& node) {
    node.bbox.clear();

    for(uint32_t inc = 0; inc < item.run; ++inc) {
        uint32_t idx = item.off + inc;
        uint32_t ref = sbvh.refs[idx];
        const Sphere& sph = (*sbvh.sphs)[ref];
        Vector3 r3(sph.radius, sph.radius, sph.radius);

        node.bbox.expand(sph.center + r3);
        node.bbox.expand(sph.center - r3);
    }
}

SpatialSplit evalChop(const SBVHItem& item, const SBVH& sbvh) {
    //check input
    assert(0 < item.run);
    
    //initialize output
    SpatialSplit split;

    //axis selection
    Vector3 dv = item.bb.max - item.bb.min;
    uint32_t axis = dv.longestAxis();
    split.axis = axis;
    //std::cerr << "a " << axis << std::endl;

    //bin all spheres
    SmitAABB binBB[BINS];
    uint32_t binCE[BINS] = {0};
    uint32_t binCX[BINS] = {0};

    //initialize all bins in axis
    real_t binwidth = (item.bb.max[axis] - item.bb.min[axis]) / BINS;
    //std::cerr << "bw " << binwidth << std::endl;

    //binning sweep
    real_t bot = 1.0 / (item.bb.max[axis] - item.bb.min[axis]);
    for(uint32_t inc = 0; inc < item.run; ++inc) {
        uint32_t idx = item.off + inc;
        uint32_t ref = sbvh.refs[idx];
        const Sphere& sph = (*sbvh.sphs)[ref];
        Vector3 r3(sph.radius, sph.radius, sph.radius);

        //calculate eid and xid
        Vector3 entry = sph.center - r3;
        Vector3 exit = sph.center + r3;

        real_t etop = entry[axis] - item.bb.min[axis];
        real_t xtop = exit[axis] - item.bb.min[axis];

        uint32_t eid = BINS*(1.0-epsilon)*bot * etop;
        uint32_t xid = BINS*(1.0-epsilon)*bot * xtop;

        //update entry & exit counts
        binCE[eid]++;
        binCX[xid]++;

        //update bin bounding boxes
        for(uint32_t bin = eid; bin <= xid; ++bin) {
            //sphere bbox
            Vector3 min = sph.center - r3;
            Vector3 max = sph.center + r3;

            //clip bbox in axis
            //TODO slight overestimation,
            //TODO implement sphere clipping
            real_t bmin = item.bb.min[axis] + bin * binwidth;
            real_t bmax = item.bb.min[axis] + (bin+1) * binwidth;
           
            //intersect not union
            min[axis] = MAX(bmin, min[axis]);
            max[axis] = MIN(bmax, max[axis]);

            //expand bb
            binBB[bin].expand(min);
            binBB[bin].expand(max);
        }
    }

    //aggregation sweeps
    real_t lA[BINS], rA[BINS];
    uint32_t lC[BINS], rC[BINS];

    uint32_t lCS = 0, rCS = 0;
    SmitAABB lBB, rBB;

    for(uint32_t bin = 0; bin < BINS; ++bin) {
        uint32_t lbin = bin, rbin = BINS-1-bin;

        //process bounds
        lBB.expand(binBB[lbin]);
        rBB.expand(binBB[rbin]);
        lA[lbin] = lBB.surfacearea();
        rA[rbin] = rBB.surfacearea();

        //process counts
        lCS += binCE[lbin];
        rCS += binCX[rbin];

        lC[lbin] = lCS;
        rC[rbin] = rCS;
    }

    //for(uint32_t bin = 0; bin < BINS; ++bin) {
    //    uint32_t lbin = bin;

        //std::cerr << "(" << lA[lbin] << " " << lC[lbin] << ") ";
    //}
    //std::cerr << std::endl;
    //for(uint32_t bin = 0; bin < BINS; ++bin) {
        //std::cerr << "(" << rA[bin] << " " << rC[bin] << ") ";
    //}
    //std::cerr << std::endl;

    //optimal cost sweep
    real_t invLC = C_LEAF / item.bb.surfacearea();

    //unroll
    split.cost = C_TRAV + (lA[0]*lC[0] + rA[0]*rC[0]) * invLC;
    split.bin = 0;

    //std::cerr << "(" << split.bin << " " << split.cost << ") ";

    //TODO check if this is correct bin -> partition way of doing it!!!
    for(uint32_t bin = 1; bin < BINS; ++bin) {
        real_t cost = C_TRAV;
        cost += (lA[bin]*lC[bin] + rA[bin]*rC[bin])*invLC;
    
        //std::cerr << "(" << bin << " " << cost << ") ";

        if(cost < split.cost) {
            split.cost = cost;
            split.bin = bin;
        }
    }

    //std::cerr << std::endl << std::endl << std::endl << std::endl;

    return split;
}

ObjectSplit evalSAH(const SBVHItem& item, const SBVH& sbvh) {
    //input check
    //std::cerr << "i " << item.off << " " << item.run << std::endl;
    assert(0 < item.run);

    //output
    ObjectSplit split;

    //axis selection
    Vector3 dv = item.bb.max - item.bb.min;
    uint32_t axis = dv.longestAxis();
    split.axis = axis;

    //binned SAH calculation
    SmitAABB binBB[BINS];
    uint32_t binCS[BINS] = {0};

    //bin spheres
    //std::cerr << "a " << split.axis << std::endl;
    //std::cerr << "v " << item.invcbs[split.axis] << std::endl;
    //std::cerr << "b ";
    for(uint32_t inc = 0; inc < item.run; ++inc) {
        //get sphere
        uint32_t idx = item.off + inc;
        uint32_t ref = sbvh.refs[idx];
        const Sphere& sph = (*sbvh.sphs)[ref];
        Vector3 r3(sph.radius, sph.radius, sph.radius);
        
        //calculate bin id
        uint32_t bid = binID(item, split, sph.center);
        //std::cerr << "(" << idx << " " << ref << " " << bid << ") ";

        //update bin
        binBB[bid].expand(sph.center - r3);
        binBB[bid].expand(sph.center + r3);

        binCS[bid]++;
    }
    //std::cerr << std::endl;

    //aggregating sweep
    real_t lA[BINS], rA[BINS];
    uint32_t lC[BINS], rC[BINS];

    SmitAABB lBB, rBB;
    uint32_t lCT = 0, rCT = 0;

    for(uint32_t bin = 0; bin < BINS; ++bin) {
        uint32_t lbin = bin, rbin = BINS-1-bin;

        //process bounds 
        lBB.expand(binBB[lbin]);
        lA[lbin] = lBB.surfacearea();

        rBB.expand(binBB[rbin]);
        rA[rbin] = rBB.surfacearea();

        //process counts
        lCT += binCS[lbin];
        lC[lbin] = lCT;

        rCT += binCS[rbin];
        rC[rbin] = rCT;
    }

    //optimal cost sweep
    //std::cerr << "isa " << item.bb.surfacearea() << std::endl;
    real_t invLC = C_LEAF / item.bb.surfacearea();

    //unroll first bin
    split.cost = C_TRAV + (lA[0]*lC[0] + rA[0]*rC[0])*invLC;
    split.bin = 0;

    //process all other bins
    for(uint32_t bin = 1; bin < BINS; ++bin) {
        //calculate cost
        real_t cost = C_TRAV;
        cost += (lA[bin]*lC[bin] + rA[bin]*rC[bin])*invLC;

        //std::cerr << cost << " ";

        //compare to minimum
        if(cost < split.cost) {
            split.cost = cost;
            split.bin = bin;
        }
    }
    //std::cerr << std::endl;
    //std::cerr << "s " << split.cost << " " << split.bin << std::endl;

    return split;
}

void partitionSAH(const ObjectSplit& split, const SBVHItem& item, SBVH& sbvh, SBVHItem& left, SBVHItem& right) {
    //partition list into two groups
    uint32_t store = item.off;
    //std::cerr << "a " << split.axis << std::endl;
    //std::cerr << "v " << item.invcbs[split.axis] << std::endl;
    //std::cerr << "p ";
    for(uint32_t inc = 0; inc < item.run; ++inc) {
        uint32_t idx = item.off + inc;
        uint32_t ref = sbvh.refs[idx];
        const Sphere& sph = (*sbvh.sphs)[ref];

        uint32_t bid = binID(item, split, sph.center);
        //std::cerr << "(" << idx << " " << ref << " " << bid << ")";

        //TODO check if this is correct bin -> partition way of doing it!!!
        if(bid < split.bin) {
            //swap reference
            uint32_t reftmp = sbvh.refs[idx];
            sbvh.refs[idx] = sbvh.refs[store];
            sbvh.refs[store] = reftmp;
            //std::cerr << "s";

            //inc itertor
            ++store;
        }
        //std::cerr << " ";
    }
    //std::cerr << std::endl;

    assert(store < (item.off + item.run));
    
    //std::cerr << "k " << store << std::endl;

    //assign left and right items
    left.off = item.off;
    left.run = store - item.off;
    //std::cerr << left.off << " " << left.run << std::endl;

    right.off = store;
    right.run = item.off + item.run - store;
    //std::cerr << right.off << " " << right.run << std::endl;

    //std::cerr << std::endl;

    //bounding boxes and centroid boxes
    SmitAABB lcb, lbb;
    for(uint32_t inc = 0; inc < left.run; ++inc) {
        uint32_t idx = left.off + inc;
        uint32_t ref = sbvh.refs[idx];
        const Sphere& sph = (*sbvh.sphs)[ref];
        Vector3 r3(sph.radius, sph.radius, sph.radius);
      
        lcb.expand(sph.center);
        lbb.expand(sph.center - r3);
        lbb.expand(sph.center + r3);
    }
    left.bb = lbb; left.cb = lcb;

    SmitAABB rcb, rbb;
    for(uint32_t inc = 0; inc < right.run; ++inc) {
        uint32_t idx = right.off + inc;
        uint32_t ref = sbvh.refs[idx];
        const Sphere& sph = (*sbvh.sphs)[ref];
        Vector3 r3(sph.radius, sph.radius, sph.radius);
      
        rcb.expand(sph.center);
        rbb.expand(sph.center - r3);
        rbb.expand(sph.center + r3);
    }
    right.bb = rbb; right.cb = rcb;
}
