#include "Parser.h"
#include "BVH.h"

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <cassert>
#include <stdio.h>
#include <omp.h>

/*void computeBBox(spheres_ct spheres, SmitAABB& bbox) {
    for(uint32_t idx = 0; idx < spheres.size(); ++idx) {
        const Sphere& sph = spheres[idx];
        Vector3 r3(sph.radius,sph.radius,sph.radius);
        Vector3 min = sph.center - r3;
        Vector3 max = sph.center + r3;

        bbox.expand(min, max);
    }
}*/

int main(int argc, char** argv) {
    /*assert(argc == 2);

    //parse
    spheres_t spheres;
    parse(argv[1], spheres);

    //bounding box
    SmitAABB bbox;
    computeBBox(spheres, bbox);

    //generate rays
    Vector3 center = (bbox.min + bbox.max) * 0.5;
    std::vector<Ray> rays;
    for(uint32_t x = 0; x < 10000; ++x) {
        Ray ray;

        ray.origin = center;
        ray.direction = randVector3().normalized();
        ray.tmax = 10000;
        
        rays.push_back(ray);
    }

    double start, end;
    uint64_t count;
    sphints_t intersects*/;

    //mkBrute
    /*printf("mkBrute---------------------------------------------\n");

    Brute bru;

    start = omp_get_wtime();
    mkBrute(spheres, bru);
    end = omp_get_wtime();

    printf("%4.3f (s)\n", end-start);

    count = 0;
    start = omp_get_wtime();
    for(uint32_t idx = 0; idx < rays.size(); ++idx) {
        bru.traverse(rays[idx], intersects);

        count += intersects.size();

        intersects.clear();
    }
    end = omp_get_wtime();

    printf("%8.8f (s) | %lu intersects | %6.1f intersects per ray\n", (end-start)/rays.size(), count, (double)count/(double)rays.size());*/

    //mkOMedBVH
    /*printf("mkOMedBVH------------------------------------------\n");

    BVH med;

    start = omp_get_wtime();
    mkOMedBVH(spheres, med);
    end = omp_get_wtime();

    printf("%4.3f (s) | %lu nodes\n", end-start, med.nodes.size());
    printf("%8.0f\n", med.SAHCost());

    count = 0;
    start = omp_get_wtime();
    for(uint32_t idx = 0; idx < rays.size(); ++idx) {
        med.traverse(rays[idx], intersects);

        count += intersects.size();

        intersects.clear();
    }
    end = omp_get_wtime();

    printf("%8.8f (s) | %lu intersects | %6.1f intersects per ray\n", (end-start)/rays.size(), count, (double)count/(double)rays.size());*/

    //mkSAHBVH
    /*printf("mkSAHBVH-------------------------------------------\n");

    BVH sah;

    start = omp_get_wtime();
    mkSAHBVH(spheres, sah);
    end = omp_get_wtime();

    printf("%4.3f (s) | %lu nodes\n", end-start, sah.nodes.size());
    printf("%8.0f\n", sah.SAHCost());

    count = 0;
    start = omp_get_wtime();
    for(uint32_t idx = 0; idx < rays.size(); ++idx) {
        sah.traverse(rays[idx], intersects);

        count += intersects.size();

        intersects.clear();
    }
    end = omp_get_wtime();
    printf("%8.8f (s) | %lu intersects | %6.1f intersects per ray\n", (end-start)/rays.size(), count, (double)count/(double)rays.size());*/
    
    //mkSBVH
    /*printf("mkSBVH---------------------------------------------\n");

    SBVH sbvh;

    start = omp_get_wtime();
    mkSBVH(spheres, sbvh);
    end = omp_get_wtime();

    printf("%4.3f (s) | %lu nodes\n", end-start, sbvh.nodes.size());
    printf("%8.0f\n", sah.SAHCost());

    count = 0;
    start = omp_get_wtime();
    for(uint32_t idx = 0; idx < rays.size(); ++idx) {
        sbvh.traverse(rays[idx], intersects);

        count += intersects.size();

        intersects.clear();
    }
    end = omp_get_wtime();
    printf("%8.8f (s) | %lu intersects | %6.1f intersects per ray\n", (end-start)/rays.size(), count, (double)count/(double)rays.size());*/
}
