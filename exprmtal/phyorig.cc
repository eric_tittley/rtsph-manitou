#include <string>
#include <cassert>
#include <cmath>

#include <omp.h>

#include "BVH.h"
#include "Kernel.h"
#include "Parser.h"

#define Y 0.274
#define XHI 1.0
#define mH 1.66e-27
#define invmH 1.0/mH
#define XHImY (XHI*(1.0-Y))

#define sx 500
#define sy 500

int main(int argc, char** argv) {
    /*Spheres sphs;
    parse("aux/data/Data_025", sphs);

    //generate tree
    BVH tree;
    mkSAHBVH(sphs, tree);

    //computation
    double avgcol = 0.0;

    #pragma omp parallel
    {
        double ncolumn = 0.0;
        double errcom = 0.0;

        leafs_t leafs;
        leafs.reserve(500);

        Vector3 min(0,0,0);
        Vector3 max(10000,10000,10000);
        Vector3 dir(0.0, 0.0, 1.0);

        #pragma omp for schedule(static,1)
        for(uint64_t v = 0; v < sy; ++v) {
            for(uint64_t u = 0; u < sx; ++u) {
                //generate ray
                real_t px = (0.5+u)/sx;
                real_t py = (0.5+v)/sy;

                Ray ray; 
                ray.direction = dir;
                ray.tmin = 0.0;
                ray.tmax = real_inf;

                ray.origin = Vector3(0, 0, -1000.0);
                ray.origin.com[0] += max.com[0] * px;
                ray.origin.com[1] += max.com[1] * py;

                //broadphase
                leafs.clear();
                tree.traverse(ray, leafs);

                //narrowphase
                for(uint32_t ileaf = 0; ileaf < leafs.size(); ++ileaf) {
                    BVHLeaf lf = leafs[ileaf];

                    //iterate spheres in leaf
                    for(uint32_t inc = 0; inc < lf.run; ++inc) {
                        uint32_t idx = lf.offset + inc;

                        //intersect
                        real_t t0, t1;
                        if(!tree.spheres.intersect(idx, ray, t0, t1)) 
                            continue;

                        //filter
                        //XXX prescision errors! use epsilon
                        if((t0 == tree.spheres.r[idx]) || 
                           (t1 == tree.spheres.r[idx])) {
                            printf("flag\n");
                            continue; //XXX flag
                        }
                        else if(-t0 > t1) { 
                            printf("s skip\n");
                            continue;
                        }

                        //calculate normalized impact parameter
                        real_t tp = (t0 + t1) * 0.5;
                        Vector3 bv;
                        ray(tp, bv);
                        bv.com[0] -= tree.spheres.cx[idx];
                        bv.com[1] -= tree.spheres.cy[idx];
                        bv.com[2] -= tree.spheres.cz[idx];
                      
                        //TODO make lookup table f(b2/h2)
                        real_t nb = bv.length() * tree.spheres.ir[idx];
                        
                        //calculate weight
                        real_t weight = kernel(nb);
                        //fixes it! close, 3.44709e22
                        //real_t weight = kernel(nb) * 2.0 * M_PI;
                        weight *= (tree.spheres.ir2[idx]);

                        //calculate column contribution
                        //XXX numerical issues
                        //ncolumn += weight;

                        //kahan summation
                        double y = weight - errcom;
                        double t = ncolumn + y;
                        errcom = (t - ncolumn) - y;
                        ncolumn = t;
                    }
                }
            }
        }

        #pragma omp critical
        {
            //XXX could have problems, should use kahan
            avgcol += ncolumn;
        }
    }

    //average
    avgcol /= (sx*sy);

    avgcol *= (invmH * XHImY * sphs.mass);
    std::cout << avgcol << std::endl;*/
}
