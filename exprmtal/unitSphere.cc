#include <iostream>
#include <limits>

#include "Spheres.h"
#include "Ray.h"
#include "Packet.h"

int main() {
    //Sphere sphere(Vector3(0,0,-1000),1.0);
    /*Spheres sphs;
    sphs.cx.push_back(0.0);
    sphs.cy.push_back(0.0);
    sphs.cz.push_back(10001);
    sphs.r.push_back(1.0);
    sphs.r2.push_back(1.0);

    Ray ray;
    ray.origin = Vector3(0,0,0);
    ray.direction = Vector3(0,0,1);
    ray.tmin = 0.0;
    ray.tmax = 10000.0;

    RayIntersect rint;
    std::cout << sphs.intersect(0, ray, rint) << std::endl;
    std::cout << rint.t0 << " " << rint.t1 << std::endl;
    std::cout << rint.nb2 << std::endl;

    Packet packet(ray);
    PacketIntersect pint;
    sphs.intersect(0, packet, pint);
    print(pint.active[0]);
    print(pint.t0[0]);
    print(pint.t1[0]);
    print(pint.nb2[0]);*/

    //POSITIVE
    //stop before entry
    /*Ray r0(Vector3(0,0,-5), Vector3(0,0,1), 3.0);
    //stop on entry, touch
    Ray r1(Vector3(0,0,-5), Vector3(0,0,1), 4.0);
    //stop inside
    Ray r2(Vector3(0,0,-5), Vector3(0,0,1), 5.0);
    //full intersect
    Ray r3(Vector3(0,0,-5), Vector3(0,0,1), 7.0);
    //start inside
    Ray r4(Vector3(0,0, 0), Vector3(0,0,1), 2.0);
    //start on exit, touch
    Ray r5(Vector3(0,0, 1), Vector3(0,0,1), 2.0);
    //start after exit
    Ray r6(Vector3(0,0, 2), Vector3(0,0,1), 3.0);

    //NEGATIVE
    //stop before entry
    Ray r7(Vector3(0,0, 5), Vector3(0,0,-1), 3.0);
    //stop on entry, touch
    Ray r8(Vector3(0,0, 5), Vector3(0,0,-1), 4.0);
    //stop inside
    Ray r9(Vector3(0,0, 5), Vector3(0,0,-1), 5.0);
    //full intersect
    Ray ra(Vector3(0,0, 5), Vector3(0,0,-1), 7.0);
    //start inside
    Ray rb(Vector3(0,0, 0), Vector3(0,0,-1), 2.0);
    //start on exit, touch
    Ray rc(Vector3(0,0,-1), Vector3(0,0,-1), 2.0);
    //start after exit
    Ray rd(Vector3(0,0,-2), Vector3(0,0,-1), 3.0);

    printf("| hit |   t0 |   t1 | tmax |\n\n");
    
    real_t t0, t1;
    bool i;

    i = sphere.intersect(r0, t0, t1);
    printf("| %3u | %4.1f | %4.1f | %4.1f |\n", i, t0, t1, r0.tmax);

    i = sphere.intersect(r1, t0, t1);
    printf("| %3u | %4.1f | %4.1f | %4.1f |\n", i, t0, t1, r1.tmax);

    i = sphere.intersect(r2, t0, t1);
    printf("| %3u | %4.1f | %4.1f | %4.1f |\n", i, t0, t1, r2.tmax);

    i = sphere.intersect(r3, t0, t1);
    printf("| %3u | %4.1f | %4.1f | %4.1f |\n", i, t0, t1, r3.tmax);

    i = sphere.intersect(r4, t0, t1);
    printf("| %3u | %4.1f | %4.1f | %4.1f |\n", i, t0, t1, r4.tmax);

    i = sphere.intersect(r5, t0, t1);
    printf("| %3u | %4.1f | %4.1f | %4.1f |\n", i, t0, t1, r5.tmax);

    i = sphere.intersect(r6, t0, t1);
    printf("| %3u | %4.1f | %4.1f | %4.1f |\n", i, t0, t1, r5.tmax);

    printf("\n");

    i = sphere.intersect(r7, t0, t1);
    printf("| %3u | %4.1f | %4.1f | %4.1f |\n", i, t0, t1, r0.tmax);

    i = sphere.intersect(r8, t0, t1);
    printf("| %3u | %4.1f | %4.1f | %4.1f |\n", i, t0, t1, r1.tmax);

    i = sphere.intersect(r9, t0, t1);
    printf("| %3u | %4.1f | %4.1f | %4.1f |\n", i, t0, t1, r2.tmax);

    i = sphere.intersect(ra, t0, t1);
    printf("| %3u | %4.1f | %4.1f | %4.1f |\n", i, t0, t1, r3.tmax);

    i = sphere.intersect(rb, t0, t1);
    printf("| %3u | %4.1f | %4.1f | %4.1f |\n", i, t0, t1, r4.tmax);

    i = sphere.intersect(rc, t0, t1);
    printf("| %3u | %4.1f | %4.1f | %4.1f |\n", i, t0, t1, r5.tmax);

    i = sphere.intersect(rd, t0, t1);
    printf("| %3u | %4.1f | %4.1f | %4.1f |\n", i, t0, t1, r5.tmax);*/
}
