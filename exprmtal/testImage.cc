#include "Image.h"

int main() {
    Image image(500, 500);

    for(size_t ix = 0; ix < 500; ++ix) {
        for(size_t iy = 0; iy < 500; ++iy) {
            if((ix % 5 == 0) && (iy % 6 == 0)) {
                image.setValue(ix, iy, 1.0);
            }
            else
                image.setValue(ix, iy, 0.0);
        }
    }

    image.write("out.pgm");
}
