#include "BVH.h"

#include <cassert>
#include <stack>
#include <algorithm>

#define MAX_LEAFS 24 

struct compSphere {
    uint32_t idx;
    spheres_ct* sphs;

    bool operator()(const uint32_t i, const uint32_t j) const {
        const Sphere& s1 = (*sphs)[i];
        const Sphere& s2 = (*sphs)[j];
        return (s1.center[idx] < s2.center[idx]);
    }
};

uint32_t partitionOM(const Item& item, BVH& bvh, const BVHNode& node) {
    //Median Cut along longest axis
    Vector3 dv = node.bbox.max-node.bbox.min;

    sphrefs_t::iterator minit = bvh.refs.begin() + item.off;
    sphrefs_t::iterator maxit = bvh.refs.begin() + item.off + item.run;

    //Find longest axis
    compSphere comp;
    comp.sphs = bvh.sphs;
    if((dv.x>dv.y)&&(dv.x>dv.z)) comp.idx = 0;
    else if(dv.y>dv.z) comp.idx = 1;
    else comp.idx = 2;

    //Sort along axis
    std::sort(minit, maxit, comp);

    //Return object median
    return item.off + item.run/2;
}


void mkOMedBVH(spheres_ct& spheres, BVH& bvh) {
    //check input
    assert(spheres.size() > 0);

    //reference spheres
    bvh.sphs = &spheres;
    for(uint32_t i = 0; i < bvh.sphs->size(); ++i) {
        bvh.refs.push_back(i);
    }

    //construct Object Median BVH
    std::stack<Item> stack;
   
    //insert root item
    Item rtitem;
    rtitem.off = 0; rtitem.run = bvh.refs.size();
    rtitem.right = false; rtitem.root = true;
    stack.push(rtitem);
   
    //build all items
    while(!stack.empty()) {
        //item
        Item item = stack.top();
        stack.pop();

        //create child
        uint32_t inode = bvh.nodes.size();
        BVHNode node;
        
        //set parent indices
        if(!item.root) {
            if(item.right)
                bvh.nodes[item.pnode].right = inode;
            else
                bvh.nodes[item.pnode].left = inode;
        }

        //calculate bounding box
        calcBndBox(item, bvh, node);

        //leaf, termination criteria
        if(item.run <= MAX_LEAFS) {
            node.leaf = true;
            node.off = item.off;
            node.run = item.run;
        }
        //internal node
        else {
            node.leaf = false;

            //partition
            uint32_t k = partitionOM(item, bvh, node);
            
            //create left item
            Item litem; 
            litem.off = item.off; 
            litem.run = k - item.off;
            litem.pnode = inode; 
            litem.right = false;

            //create right item
            Item ritem; 
            ritem.off = k;
            ritem.run = item.off+item.run-k;
            ritem.pnode = inode; 
            ritem.right = true;

            //FILO
            stack.push(ritem); stack.push(litem);
        }

        //write node
        bvh.nodes.push_back(node);
    }
}
