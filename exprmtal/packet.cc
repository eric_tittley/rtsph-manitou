#include <string>
#include <cassert>
#include <cmath>

#include <omp.h>

#include "BVH.h"
#include "Kernel.h"
#include "Parser.h"
#include "Image.h"
#include "Worker.h"

#define sx 400
#define sy 400

#include <iostream>

int main(int argc, char** argv) {
    /*std::clog << "parsing" << std::endl;
    Spheres sphs;
    parse("aux/data/Data_025", sphs);

    //generate tree
    std::clog << "building" << std::endl;
    BVH tree;
    mkSAHBVH(sphs, tree);

    //computation
    Image image(sx, sy);

    std::cout << "calculating" << std::endl;
    double st = omp_get_wtime();

    //#pragma omp parallel default(none) shared(image, tree)
    //{

    Vector3 min(0,0,0);
    Vector3 max(10000,10000,10000);
    Vector3 dir(0.0, 0.0, 1.0);
    
    Vector3 d = max; d -= min;

    double4 dx, dy;
    dx = _mm256_set1_pd(d.com[0]);
    dy = _mm256_set1_pd(d.com[1]);

    leafs_t leafs;
    leafs.reserve(1000);

    //#pragma omp for schedule(static,1)
    for(uint64_t iy = 0; iy < sy; iy += 2) {
        for(uint64_t ix = 0; ix < sx; ix += 2) {
            //generate ray packet
            double pxh = (ix+0.5)/(double)sx;
            double pyh = (iy+0.5)/(double)sy;
            double pxf = (ix+1.5)/(double)sx;
            double pyf = (iy+1.5)/(double)sy;

            RPacket packet;
            //packet origin
            packet.ox = _mm256_set1_pd(min.com[0]);
            packet.oy = _mm256_set1_pd(min.com[1]);
            packet.oz = _mm256_set1_pd(min.com[2]);
            __m256d ptx = _mm256_setr_pd(pxh, pxf, pxh, pxf);
            __m256d pty = _mm256_setr_pd(pyh, pyh, pyf, pyf);
            __m256d offx = _mm256_mul_pd(dx, ptx);
            __m256d offy = _mm256_mul_pd(dy, pty);
            packet.ox = _mm256_add_pd(packet.ox, offx);
            packet.oy = _mm256_add_pd(packet.oy, offy);
            
            //packet directions
            packet.dx = _mm256_set1_pd(0.0);
            packet.dy = _mm256_set1_pd(0.0);
            packet.dz = _mm256_set1_pd(1.0);

            //packet length
            packet.tmin = _mm256_set1_pd(0.0);
            packet.tmax = _mm256_set1_pd(15000.0);

            //broadphase
            leafs.clear();
            tree.traverse(packet, leafs);

            //narrowphase
            double4 cdensity = _mm256_setzero_pd();
            for(uint32_t ileaf = 0; ileaf < leafs.size(); ++ileaf) {
                BVHLeaf lf = leafs[ileaf];

                //iterate spheres in leaf
                //TODO can we loop unroll?
                for(uint32_t inc = 0; inc < lf.run; ++inc) {
                    uint32_t idx = lf.offset + inc;
                    //intersect sphere
                    PIntersect itr = tree.spheres.intersect(idx, packet);
                    //filter ray
                    double4 nt0 = _mm256_sub_pd(zero, itr.t0);
                    double4 mask = _mm256_cmp_pd(nt0, itr.t1, _CMP_LE_OQ);
                    itr.active = _mm256_and_pd(itr.active, mask);

                    //check if packet is still alive
                    if(_mm256_movemask_pd(itr.active) == 0) continue;

                    //calculate normalized impact parameter
                    double4 nb = impactparameter(packet, tree.spheres, idx, itr.t0, itr.t1);

                    //mask, such that 0.0 <= nb <= 1.0
                    //avoid segfault due to >1
                    nb = _mm256_and_pd(nb, itr.active);

                    //calculate weight
                    double4 kern = kernel(nb);

                    double4 ir2 = _mm256_set1_pd(tree.spheres.ir2[idx]);

                    //kernel -> weight
                    double4 weights = _mm256_mul_pd(kern, ir2);
                    //mask final result
                    weights = _mm256_and_pd(weights, itr.active);
                    
                    //sum all
                    cdensity = _mm256_add_pd(cdensity, weights);
                }
            }

            //write column densities into image
            double dts[4] __attribute__((aligned(16))); 
            _mm256_store_pd(dts, cdensity);

            image.setValue(ix  , iy  , log(dts[0]));
            image.setValue(ix+1, iy  , log(dts[1]));
            image.setValue(ix  , iy+1, log(dts[2]));
            image.setValue(ix+1, iy+1, log(dts[3]));
        }
    }
    //}

    double en = omp_get_wtime();
    std::cout << std::endl;

    std::clog << "outputing" << std::endl;
    image.write("packet.pbm");
    printf("TT  %0.6f\n", (en-st));
    printf("TPR %0.6f\n", ((en-st)/((double)sx*(double)sy)));*/
}
