#include "Common.h"
#include "Spheres.h"
#include "Parser.h"
#include "BVH.h"
#include "Rays.h"

#include <cstdlib>

#define RAYS 128

int main() {
    //parse sph dataset
    /*Spheres sphs;
    parse("aux/data/Data_025", sphs);

    //build tree
    BVH tree;
    mkSAHBVH(sphs, tree);

    //generate rays
    srand(1234);
    Rays rays;
    for(size_t iray = 0; iray < RAYS; ++iray) {
        Vector3 p1(randreal()*10000.0, 
                   randreal()*10000.0, 
                   randreal()*10000.0);
        Vector3 p2(randreal()*10000.0, 
                   randreal()*10000.0, 
                   randreal()*10000.0);

        rays.setFromPoints(p1, p2);
    }

    for(size_t iray = 0; iray < RAYS; iray += 16) {
        size_t reftotal = 0;
        size_t treetotal = 0;
        
        for(size_t isub = 0; isub < 16; ++isub) {
            //reference search
            Ray ray; rays.get(iray+isub, ray);

            RayIntersect intersect;
            for(size_t isph = 0; isph < sphs.count(); ++isph) {
                if(sphs.intersect(isph, ray, intersect)) {
                    ++reftotal;
                }
            }

            //tree search
            treetotal += tree.intersect(ray);
        }
        std::cout << "IRAY: " << iray << std::endl;
        std::cout << "REF:  " << reftotal << std::endl;
        std::cout << "TREE: " << treetotal << " ";

        //packet search
        Rays pcp;
        for(size_t isub = 0; isub < 16; ++isub) {
            size_t idx = iray + isub;
            
            pcp.ox.push_back(rays.ox[idx]);
            pcp.oy.push_back(rays.oy[idx]);
            pcp.oz.push_back(rays.oz[idx]);
            pcp.dx.push_back(rays.dx[idx]);
            pcp.dy.push_back(rays.dy[idx]);
            pcp.dz.push_back(rays.dz[idx]);
            pcp.tmin.push_back(rays.tmin[idx]);
            pcp.tmax.push_back(rays.tmax[idx]);
        }
        Packet packet(pcp);

        //compute
        size_t packtotal = 0;
        size_t packnodes = 0;

        size_t counts[PACKET_COUNT];
        tree.intersect(packet, counts);
        std::cout << "PACK: " << packtotal << std::endl;
    }*/
}
