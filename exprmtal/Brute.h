#pragma once

#include "Common.h"
#include "Tree.h"

class Brute { //: public Tree {
public:
    void traverse(ray_ct&, sphints_t&) const;
    void print() const;

    spheres_ct* sphs;
};

void mkBrute(spheres_ct&, Brute&);
