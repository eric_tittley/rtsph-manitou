# an architecture where -march=native turns on -mavx is prefered.

.SUFFIXES: ;
.SECONDARY: ;

vpath %.cc src units tests examples

HEADERS:=$(wildcard include/*.h)
SOURCES:=$(wildcard src/*.cc)
OBJECTS:=$(patsubst %.cc, build/%.o, $(notdir $(SOURCES)))

SRCUNITS:=$(wildcard units/*.cc)
UNITS:=$(patsubst %.cc,bin/%,$(notdir $(SRCUNITS)))

SRCTESTS:=$(wildcard tests/*.cc)
TESTS:=$(patsubst %.cc,bin/%,$(notdir $(SRCTESTS)))

SRCEXPLS:=$(wildcard examples/*.cc)
EXPLS:=$(patsubst %.cc,bin/%,$(notdir $(SRCEXPLS)))

all: $(UNITS) $(TESTS) $(EXPLS)

lib: build bin librtsph.a

librtsph.a: $(OBJECTS)
	$(AR) $(AR_FLAGS) r librtsph.a $(OBJECTS)

units: $(UNITS) ;

tests: $(TESTS) ;

examples: $(EXPLS) ;

%: $(addprefix bin/,%) ;

%.o: $(addprefix build/,%.o) ;

bin/%: build/%.o $(OBJECTS)
	$(CXX) $(LDFLAGS) $< $(OBJECTS) -o $@

build/%.o: %.cc $(HEADERS)
	$(CXX) $(CXXFLAGS) -Iinclude -c $< -o $@

build:
	mkdir build

bin:
	mkdir bin

.PHONY: clean
clean: clean_bin clean_build clean_lib

.PHONY: clean_bin
clean_bin:
	-rm bin/*
	-rm -fr bin

.PHONY: clean_build
clean_build:
	-rm build/*
	-rm -fr build

.PHONY: clean_lib
clean_lib:
	-rm librtsph.a

.PHONY: distclean
distclean: clean_lib clean_build clean_bin
	
